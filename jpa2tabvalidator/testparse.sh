#!/bin/bash

#PARAM=" example/tab/classa.xlsx example/model.gen.test1.jar  vstatus.json" 

#error
PARAM="  example/tab/lipmuscl.xlsx example/jpa_generated_test2.jar vstatus.json"

#validated
#PARAM="  example/tab/lipmusclOK.xlsx example/jpa_generated_test2.jar vstatus.json"

#PARAM=" example/tab/baseanalyseDateIssue.xlsx  example/generated_model.jar vstatus.json"

#PARAM=" -n example/tab/classa.xlsx -t oo -g example/model.gen.test1.jar"


bash parsetab.sh $PARAM
