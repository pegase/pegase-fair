package tabvalidator;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

public class CustomValidationException 
extends ValidationException {

   private String exceptionOriginClassName;

	public String getExceptionOriginClassName() {
	return exceptionOriginClassName;
}

public void setExceptionOriginClassName(String exceptionOriginClassName) {
	this.exceptionOriginClassName = exceptionOriginClassName;
}

	private int rowIndex;
	private int columnIndex;
 	private List<String> rowRawContent=new ArrayList<String>() ;

	public CustomValidationException(
			String msg, 
			int rowIndex,
			int columnIndex,
			List<String> rowRawContent
			) {
	   super(msg);
	   this.rowIndex=rowIndex;
	   this.columnIndex=columnIndex;
	   this.rowRawContent=rowRawContent;
	   this.exceptionOriginClassName=this.getClass().getSimpleName();
	}
	
	public CustomValidationException(
			String msg, 
			int rowIndex,
			int columnIndex,
			List<String> rowRawContent,
			Exception e
			) {
	   super(msg);
	   this.rowIndex=rowIndex;
	   this.columnIndex=columnIndex;
	   this.rowRawContent=rowRawContent;
	   this.exceptionOriginClassName=e.getClass().getSimpleName();
	   this.setStackTrace(e.getStackTrace());
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getColumnIndex() {
		return columnIndex;
	}

	public void setColumnIndex(int columnIndex) {
		this.columnIndex = columnIndex;
	}

	public List<String> getRowRawContent() {
		return rowRawContent;
	}

	public void setRowRawContent(List<String> rowRawContent) {
		this.rowRawContent = rowRawContent;
	}

}
