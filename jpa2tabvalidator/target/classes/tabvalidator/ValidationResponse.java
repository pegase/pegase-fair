package tabvalidator;

import java.util.ArrayList;
import java.util.List;

public class ValidationResponse {
	
	   private Integer row;

	   private Integer col;
	   
       public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	  private String trace;
	   private String status;
	   private String infoMessage;
	   private String exceptionClassName;
	   
	   
	 	private List<String> rowContent=new ArrayList<String>() ;

	   public List<String> getRowContent() {
		return rowContent;
	}

	public void setRowContent(List<String> rowContent) {
		this.rowContent = rowContent;
	}

	public ValidationResponse() {
		   
	   }
	   
	   public ValidationResponse(
			   String status, 
			   String infoMessage,
			   String  exceptionClassName
         ) {
	      super();
	      this.status = status;
	      this.infoMessage = infoMessage;
	      this.exceptionClassName= exceptionClassName;

	   }
	   
	   public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getCol() {
		return col;
	}

	public void setCol(Integer col) {
		this.col = col;
	}

	   
	   public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInfoMessage() {
		return infoMessage;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getExceptionClassName() {
		return exceptionClassName;
	}

	public void setExceptionClassName(String exceptionClassName) {
		this.exceptionClassName = exceptionClassName;
	}

	 
	}