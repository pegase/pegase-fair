package tabvalidator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.bind.ValidationException;

import org.apache.commons.cli.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ValidateEntityFromFile {

	public static void main(String[] args) throws Exception {

		Options options = new Options();

		// Option set
		Option filename = new Option("n","name",true,"filename");
		filename.setRequired(true);
		options.addOption(filename);

		Option filetype = new Option("t","type",true,"spreadsheet file type");
		filetype.setRequired(true);
		options.addOption(filetype);

		Option pack = new Option("g","generatejar",true,"jar file with JPA classes used for validation / template creation");
		pack.setRequired(true);
		options.addOption(pack);
		
		Option jsono  = new Option("j","json-response",true,"output a json file with detailed validation status and exception");
		jsono.setRequired(false);
		options.addOption(jsono);

		// Parse the arguments
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			formatter.printHelp("argument names", options);
			System.exit(1);
			return;
		}

		// Get arguments options 
		String inputFileName = cmd.getOptionValue("name");	
		String inputFileType = cmd.getOptionValue("type");	
		String generatejar = cmd.getOptionValue("generatejar");
		String jsonFile = cmd.getOptionValue("json-response");
		
		
		ReadWorkBook f = new ReadWorkBook(inputFileName,inputFileType,generatejar);
		File file = new File(inputFileName);

		// Read the given input file or print an error
		if ( !(inputFileName==null) && file.exists() ) {
			
			boolean jsonDone=false;
			try {
			   f.parseFile();
			}catch(Exception e){
				
				 
					generateJsonFile(jsonFile,e);
					jsonDone=true;
				 
				  
				 
			}
			if ( jsonDone==false) {
				
			     generateJsonFile(jsonFile,null);
			     jsonDone=true;
			}
		}
		else {
			String x = "file not found "+file.getAbsolutePath();
			die(x);
		}
	
	}


	private static void generateJsonFile(String jsonFile, Exception e) throws IOException {
		
		ValidationResponse vr = new ValidationResponse();
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder = gsonBuilder.setPrettyPrinting();
		Gson gson = gsonBuilder.create();
		String msg ="";

		if(e==null) {
			msg="validation OK";
			vr.setInfoMessage(msg);
			vr.setStatus("VALIDATED");
		}else {
			vr.setStatus("ERROR");

			
			String trace = exceptionStringTrace(e);
			vr.setTrace(trace);
			
			vr.setExceptionClassName(
					e.getClass().getSimpleName()
					);
			
			if(e instanceof CustomValidationException ) {
				CustomValidationException cve =(CustomValidationException) e ;
				msg =cve.getMessage(); 
				vr.setRow(cve.getRowIndex()+1);
				vr.setCol(cve.getColumnIndex());
				
				vr.setRowContent(cve.getRowRawContent());
				vr.setExceptionClassName(
						cve.getExceptionOriginClassName()
				);
			}
			else if(e instanceof ValidationException ) {
				  msg =e.getMessage(); 
				 
			}
	            else if(e instanceof ClassNotFoundException ) {
			      msg =e.getMessage();	 
			}
			else {
				  msg =e.getMessage();
				/*
				 ValidationException
				 InstantiationException
				 IllegalAccessException
				 InvocationTargetException
				 NoSuchFieldException
				 IOException
				 DataParsingException
				 ParseException 
				 */
			}
			 vr.setInfoMessage(msg);
		}
		String json = gson.toJson(vr);
		System.out.println("==>"+jsonFile);
		if(jsonFile!=null) {
			writeFile(json, jsonFile) ;
		}
		print(json) ;
		
	}


	private static String exceptionStringTrace(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String trace = sw.toString();
		return trace;
	}

	private static void print(String json) {
		System.out.println(json);
	}


	public static void writeFile(String content, String fileName) 
			  throws IOException {
			 
			    BufferedWriter writer = new BufferedWriter(
			    		new FileWriter(fileName));
			    writer.write(content);
			     
			    writer.close();
			}
	
	private static void die(String x) {
		System.err.println(x);
		System.exit(1);
	}
}