#!/bin/bash


 

JAVA=$(which java) 

RDIR=$(dirname $0)

#echo "RDIR:$RDIR--$0"

 
OPT=" -Dfile.encoding=UTF-8 -Djava.util.logging.config.file=commons-logging.properties "
 
LIB=$(echo $RDIR/lib/*.jar | tr ' ' ':')
#LIB=$(echo ./lib/*.jar | tr ' ' ':')

 

CPL=$RDIR/bin:$LIB


ENTRYPOINT=tabvalidator.ValidateEntityFromFile




PARAM=" -n $1 -t oo -g $2 -j $3"

echo $JAVA $OPT -classpath $CPL $ENTRYPOINT $PARAM
$JAVA $OPT -classpath $CPL $ENTRYPOINT $PARAM


if [ $? -eq 0 ]
then
  echo "Successfully execution"
  exit 0
else
  echo "command line error" >&2
  exit 1
fi

