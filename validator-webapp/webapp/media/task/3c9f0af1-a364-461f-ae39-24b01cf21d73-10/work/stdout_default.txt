/usr/bin/java -Dfile.encoding=UTF-8 -Djava.util.logging.config.file=commons-logging.properties -classpath /parsetab/bin:/parsetab/lib/antlr-2.7.6.jar:/parsetab/lib/args4j-2.33.jar:/parsetab/lib/classmate-0.5.4.jar:/parsetab/lib/commons-beanutils-1.9.3.jar:/parsetab/lib/commons-cli-1.4.jar:/parsetab/lib/commons-codec-1.10.jar:/parsetab/lib/commons-collections-3.2.2.jar:/parsetab/lib/commons-collections4-4.1.jar:/parsetab/lib/commons-lang-2.6.jar:/parsetab/lib/commons-logging-1.2.jar:/parsetab/lib/curvesapi-1.04.jar:/parsetab/lib/dom4j-1.6.1.jar:/parsetab/lib/gson-2.8.6.jar:/parsetab/lib/hamcrest-core-1.3.jar:/parsetab/lib/hibernate-annotations-3.5.6-Final.jar:/parsetab/lib/hibernate-commons-annotations-3.2.0.Final.jar:/parsetab/lib/hibernate-core-3.5.6-Final.jar:/parsetab/lib/hibernate-jpa-2.0-api-1.0.1.Final.jar:/parsetab/lib/hibernate-validator-6.0.8.Final.jar:/parsetab/lib/javax.el-2.2.4.jar:/parsetab/lib/javax.el-api-3.0.0.jar:/parsetab/lib/javax.persistence-api-2.2.jar:/parsetab/lib/jboss-logging-3.3.1.Final.jar:/parsetab/lib/jta-1.1.jar:/parsetab/lib/junit-4.12.jar:/parsetab/lib/persistence-api-1.0.jar:/parsetab/lib/poi-3.17.jar:/parsetab/lib/poi-ooxml-3.17.jar:/parsetab/lib/poi-ooxml-schemas-3.17.jar:/parsetab/lib/slf4j-api-1.5.8.jar:/parsetab/lib/stax-api-1.0.1.jar:/parsetab/lib/validation-api-2.0.1.Final.jar:/parsetab/lib/xml-apis-1.0.b2.jar:/parsetab/lib/xmlbeans-2.6.0.jar:/parsetab/lib/xref_util.jar tabvalidator.ValidateEntityFromFile -n /var/lib/cwl/stg4c04a4d4-16ee-4726-832a-bf8b8af73db8/00-00-1597881600_endpointassay_Exemple_NA_error.xlsx -t oo -g /var/lib/cwl/stgb308ba02-73ef-4cab-961d-a4bb4870783c/model.gen.pegase_v1.jar -j vstatus.json
==>vstatus.json
{
  "row": 6,
  "col": 18,
  "trace": "tabvalidator.CustomValidationException: For input string: \"NA\"\n\tat sun.misc.FloatingDecimal.readJavaFormatString(FloatingDecimal.java:2043)\n\tat sun.misc.FloatingDecimal.parseFloat(FloatingDecimal.java:122)\n\tat java.lang.Float.parseFloat(Float.java:451)\n\tat java.lang.Float.\u003cinit\u003e(Float.java:532)\n\tat tabvalidator.ReadWorkBook.defineValueType(ReadWorkBook.java:410)\n\tat tabvalidator.ReadWorkBook.parseSheet(ReadWorkBook.java:213)\n\tat tabvalidator.ReadWorkBook.parseFile(ReadWorkBook.java:119)\n\tat tabvalidator.ValidateEntityFromFile.main(ValidateEntityFromFile.java:71)\n",
  "status": "ERROR",
  "infoMessage": "For input string: \"NA\"",
  "exceptionClassName": "NumberFormatException",
  "rowContent": [
    "12-Jun-2019",
    "sdare",
    "MO-LAB-006",
    "Dosage enzymatique du glucose par la méthode utilisant la glucose oxydase sur l’analyseur multiparamétrique Konélab20i",
    "Konélab20",
    "THERMO; 981304;sCal 981831;Nortrol 981043;Abtrol 981044",
    "S415;G716;H342;H654",
    "Konélab20",
    "PEGASE208837",
    "340.0",
    "0-3400",
    "mg/mL",
    "0.999",
    "0.0005",
    "2.0",
    "mg",
    "1.0",
    "0.33513",
    "NA"
  ]
}
Successfully execution
