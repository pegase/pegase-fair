---
label: "skeleton_label"
inputs:
  w_model_gen_pegase_v1_jar:
    id: "w_model_gen_pegase_v1_jar"
    type: "File"
    doc: "worflow input"
  w_00-00-1597881600_endpointassay_Exemple_NA_error_xlsx:
    id: "w_00-00-1597881600_endpointassay_Exemple_NA_error_xlsx"
    type: "File"
    doc: "worflow input"
outputs:
  w_output_all:
    id: "w_output_all"
    outputSource: "s1/t_file_glob"
    outputBinding:
      glob: "*"
    type:
      type: "array"
      items: "File"
    doc: "workflow output"
hints: []
requirements: []
cwlVersion: "v1.0"
steps:
  s1:
    id: "s1"
    run:
      label: null
      inputs:
        t_model_gen_pegase_v1_jar:
          id: "t_model_gen_pegase_v1_jar"
          inputBinding:
            position: 3
          type: "File"
        t_00-00-1597881600_endpointassay_Exemple_NA_error_xlsx:
          id: "t_00-00-1597881600_endpointassay_Exemple_NA_error_xlsx"
          inputBinding:
            position: 2
          type: "File"
      outputs:
        t_file_glob:
          id: "t_file_glob"
          outputBinding:
            glob: "*"
          type:
            type: "array"
            items: "File"
      hints:
      - dockerPull: "fjrmore/parsetab"
        class: "DockerRequirement"
      requirements:
      - listing:
        - entryname: ".mywrapper.sh"
          entry: "#/bin/sh\n\n\nINP1=$1\nINP2=$2\n\nbash /parsetab/parsetab.sh $INP1\
            \ $INP2 vstatus.json \n"
        class: "InitialWorkDirRequirement"
      - class: "InlineJavascriptRequirement"
      cwlVersion: "v1.0"
      stdin: null
      stdout: "stdout_default.txt"
      stderr: null
      baseCommand:
      - "/bin/sh"
      - ".mywrapper.sh"
      arguments: []
      class: "CommandLineTool"
      doc: null
    in:
      t_model_gen_pegase_v1_jar: "w_model_gen_pegase_v1_jar"
      t_00-00-1597881600_endpointassay_Exemple_NA_error_xlsx: "w_00-00-1597881600_endpointassay_Exemple_NA_error_xlsx"
    out:
    - "t_file_glob"
    hints: []
    requirements: []
class: "Workflow"
doc: "skeleton"
