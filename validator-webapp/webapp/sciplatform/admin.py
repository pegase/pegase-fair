from django.contrib import admin
from django.contrib.auth import admin as auth_admin
 
from django.db import models
from  .models import Dataset

"""

class DatasetInline(admin.TabularInline):
    model = Dataset

class DatasetAdmin(admin.ModelAdmin):
    inlines = [
        DatasetInline,
    ]
"""
admin.site.register(Dataset) 
