from __future__ import unicode_literals

from mako.template import Template
from mako.runtime import Context
from io  import StringIO

###usage
"""
from sciplatform.templateManager import CTemplateEngine
cte=CTemplateEngine()


contextmap=dict()
contextmap["para_datamodel"]="/data/model/pegase.jar"
contextmap["tab"]="/tmp/excl.xls"
contextmap=cte.removePrefix(contextmap)

cmdt= "bash /parsetab/parsetab.sh   ${tab}   ${datamodel}  vstatus.json $aa ${aaa}"

cmd=cte.renderTemplate(cmdt,contextmap)
print(cmd)

"""


class CTemplateEngine():

   def __init__(self):
       
        self.engine= "mako"
  


   def removePrefix(self,ctmap):

          m=dict()

          for k in ctmap.keys():
             tkey=k
             if tkey.startswith("data_"):
                tkey=tkey.replace('data_','')

             elif tkey.startswith("para_"):
                tkey=tkey.replace('para_','')

             m[tkey]=ctmap[k]
          return m


   def renderTemplate(self,cmdtm,ctmap):

          contextmap=dict()
          cmdLtmp=cmdtm.split()
          cmdL=list()
          for tk in cmdLtmp:
             tk=tk.strip()
             if tk.startswith("$"):
                tkey=tk.replace('$','').replace('{','').replace('}','')
                valvar="${"+tkey+"}"
                if tkey in ctmap.keys():
                   contextmap[tkey]=ctmap[tkey]
                else:
                   contextmap[tkey]= valvar
                tk=valvar 

             cmdL.append(tk)    
             #print(tk)
          mytemplate = Template(" ".join(cmdL))
          buf = StringIO()
          #ctx = Context(buf, datamodel="/data/model/pegase.jar", tab="/tmp/excl.xls")
          ctx = Context(buf, **contextmap )
          mytemplate.render_context(ctx)
          r=buf.getvalue()
          return r





