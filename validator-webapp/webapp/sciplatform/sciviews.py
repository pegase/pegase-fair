#Author
#FM  09/2019
from __future__ import unicode_literals

 
import requests
import logging
import json
import os
import sys
import datetime
import time
from shutil import move
import random

from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone

import django_keycloak.services.oidc_profile
import django_keycloak.services.remote_client


from django.contrib.auth import get_user_model
 
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views import generic
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.http import HttpResponse, JsonResponse
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from django.contrib.sites.shortcuts import get_current_site


from sciplatform.utils import tojson
from sciplatform.forms import UploadDocumentForm,ValidatorUploadDocumentForm

from sciplatform.models import Dataset
from sciplatform.templateManager import CTemplateEngine
 
#################KC+


from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    PermissionRequiredMixin
)
from django.views.generic.base import TemplateView
from jose.exceptions import JWTError

import django_keycloak.services.oidc_profile
import django_keycloak.services.remote_client


from sciplatform.jwtutils import CustomJWTManager
from sciplatform.jtesapi import JTESClient
from sciplatform import context_processors
logger = logging.getLogger(__name__)









class JWTAuth(PermissionRequiredMixin, TemplateView):

    raise_exception = True

    permission_required = 'ADMIN' 
    

    def get_context_data(self, **kwargs):

        jmanager=   CustomJWTManager(self.request)
        
        latoken=jmanager.userEncodedJWTAccessToken()
 
        rolelist= jmanager.confRoleListFromAccessToken()

         
        atoken=jmanager.userEncodedJWTAccessToken()
    
        rolelist= jmanager.confRoleListFromAccessToken()

 
        
  
        # Call the base implementation first to get a context
        context = super(JWTAuth, self).get_context_data(
               permissions=rolelist,
               **kwargs)
        
        context['latoken'] = latoken
    
        context['rolelist_str'] = tojson(rolelist)
        context['permission_required'] = str(self.permission_required)

        return context 
 
#################KC-
 

class JWTAuthUpload(JWTAuth):

   def __init__(self, **kwargs):
        super().__init__() 

        self.paramd= dict()
        self.image= None
        self.cmdTemplate= None
         
        self.tengine=CTemplateEngine()

        self.jtesclientImpl=None 
        self.waitStatus =[
            "UNKNOWN",
            "QUEUED",
            "INITIALIZING",
            "RUNNING",
            "PAUSED"
            ]
        
        self.errorStatus =[
            "EXECUTOR_ERROR",
            "SYSTEM_ERROR",
            "CANCELLED"
            ]
        self.doneStatus =[
            "COMPLETE" 
            ]
        for key, value in kwargs.items():
            setattr(self, key, value)
        self.initialize()
        

   def  initialize(self):
        
        self.image= "python:2-alpine"
        self.cmdTemplate= "wc -m -l %s | cut -f 2 -d ' ' "
 

   def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        if context["form"].is_valid():
            print('context["form"].is_valid()')
            #save your model
            #redirect

        return super(TemplateView, self).render_to_response(context)


   def get_context_data(self, **kwargs):

        context = super(JWTAuthUpload, self).get_context_data( **kwargs)
  
        ar=self.processInputRequest(self.request,context)
        form=ar[0]
        context=ar[1]
        context['zz'] = "zzz"
        context['form'] = form
    
        return context 

   def jtesclient(self):

        if self.jtesclientImpl is None:
           jtesurl=settings.JTES_URL
           jtesauth=settings.JTES_AUTH
           jtescl= JTESClient(self.request, jtesurl,jtesauth)
           self.jtesclientImpl=jtescl
        
        return self.jtesclientImpl

   def inputform(self,request,context):
    form = UploadDocumentForm()
    if request.method == 'POST':
        form = UploadDocumentForm(request.POST, request.FILES)   
    return form





   def displayParamD(self):
       for k in self.paramd.keys():
            print("*** %s =%s" % (k,self.paramd[k]))


   def processInputRequest(self,request,context):

    form = self.inputform(request,context)
   
    if form.is_valid():


         self.paramd=self.defineParamFromRequest(request)

         dsm=self.uploadInputFiles(request)
         if dsm is not None:
           for fk in dsm.keys():
              self.paramd[fk]=dsm[fk]
           dsl=dsm.values()
         else:
           dsl=None

         self.displayParamD()

         
        
         context= self.dataProcessing(request,context, dsl)

    return [form,context]



   def dataProcessing(self,request,context, dsl):
      #/app/sciplatform/media/upload/2020-01-27/00-00-1580083200_hplchydrolisedaa.xlsx" does not exist
   

       
         #current_domain = get_current_site(request)
         schedomain =settings.DOMAIN_FOR_LINK
         padl=settings.PATH_DOWNLOAD_LINK
         p2d=settings.PATH_2_DOWNLOAD_LINK
         schedomain=schedomain.rstrip('/')
         padl=padl.lstrip('/')
         padl=padl.rstrip('/')
         schedomain=schedomain+"/"+padl+"/"

         context["schedomain"]=schedomain
         context["isprocessdone"]=False
         context["file_url"]=""
         context["toolerror"]=False
         if dsl  is not None:
            lsz=len(dsl)
            ix=0
            for ds in dsl:              
                ix+=1
                if ix==lsz:
                   sp=""
                else:
                   sp=","

                file_url=ds.path.rstrip('/')+"/"+sp.lstrip('/').rstrip('/')
                file_url=file_url.replace("//","/")
                file_url=file_url.replace(p2d,"",1)
                file_url=file_url.lstrip('/').rstrip('/')
                

                context["file_url"]+=file_url

            vreport=self.processDs(dsl)

            context["isprocessdone"]=True
            context["status"]=vreport["status"]
            context["iserror"]=False
            context["errorMessage"]=""
            if vreport["status"] in self.errorStatus:
               context["iserror"]=True
               context["errorMessage"]=" error message : %s " % ( str(vreport["status"] ))
       
 

            if vreport["validated"] ==True:
                  for ds in dsl:   
                      ds.save()

            if vreport["res"] is not None:
                  context["jobid"]=vreport["res"]["jobid"]
                  context["state"]=vreport["res"]["state"]
                  jstr=vreport["atoolresponse"]

                  print("---1")
                  print(jstr)
                  print("---1--")
                  try:
                     js=json.loads(jstr)
                     if "status" in js:
                        context["toolstatus"]=js["status"]
                     if "infoMessage" in js:
                        context["toolinfoMessage"]=js["infoMessage"]
                     if "exceptionClassName" in js:
                        context["toolexceptionClassName"]=js["exceptionClassName"]
                     if "trace" in js:
                        context["tooltrace"]=js["trace"]
                     if "rowContent" in js:
                        context["toolrowContent"]=js["rowContent"]
                        context["toolrowContentstr"]=str(context["toolrowContent"])
                     if "row" in js:
                        context["toolrow"]=js["row"]
                     if "col" in js:
                        context["toolcol"]=js["col"]


                     context["toolresponse"]=jstr  

                     if context["toolstatus"] =="VALIDATED":
                                    context["toolerror"]=False
                     else:
                                    context["toolerror"]=True
                                    context["errorMessage"]=" tool error message : %s ,  exception:  %s   trace: %s, " % ( str(context["toolinfoMessage"]), str(context["toolexceptionClassName"]) , str(context["tooltrace"])    )
   


                  except:
                     context["toolerror"]=True
                     context["toolstatus"]=None
                     context["toolinfoMessage"]=None
                     context["toolexceptionClassName"]=None
                     context["tooltrace"]="python:"+str(sys.exc_info())
                     context["toolrowContent"]=None
                     context["errorMessage"]=context["tooltrace"]
                     context["toolresponse"]=jstr  
            else:
                  context["jobid"]=""
                  context["state"]=""
                  context["toolresponse"]=""

         return context


   def processDs(self,dsl): 

      vreport=dict()

      
      vreport["toolresponse"]="NA"
      vreport["validated"] =False
      vreport["message"] ="not processing"

      idsl=list() 
      for ds in dsl:

         path=ds.path
         filename=ds.filename
         ids=dict()
         ids["filename"] =filename
         ids["path"] =path
         ids["url"] ="todo:url_to_upload_service"
         idsl.append(ids)

      vreport["inputds"]=idsl

      vreport=self.processWithAPI(dsl,vreport)
      if vreport["res"] is not None:
          jobid=vreport["res"]["jobid"]
          vreport=self.waitTaskEndWithAPI(jobid,vreport)
      return vreport




   def waitTaskEndWithAPI(self,jobid,vreport):



        maxIter=200
        goon=True
        i=0
        while goon==True and i < maxIter:
           i+=1
           res=self.statusTaskWithAPI(jobid)
           #print("!:!:!:"+str(res))
           jobid=res["jobid"]           
           state=res["state"] 
           print("=====jobid %s, status %s\n %s" %(jobid, state, res) )         
           vreport["res"]=res
           if state not in  self.waitStatus:
              goon=False
              vreport["status"]=state
              if state == "COMPLETE":
                 outputs=res["outputs"]

                 for outp in outputs:
                     dataurl=outp["url"]
                     datapath=outp["path"]
                     filename=os.path.basename(datapath)
                     #read data 
                     with open(datapath, 'r') as reader:
                       cnt= reader.read()
                     #cnt =self.getFileWithAPI(jobid,filename) 
                     print("!!!!!!!CONTENT:"+str(cnt))
                     outp["content"]=str(cnt)
                     vreport["toolresponse"]=""+str(cnt)+""
                     atoolr=self.analyseToolResponse(str(cnt))
                     vreport["atoolresponse"]=atoolr

                 vreport["outputs"]=outputs


              #if state  in self.errorStatus  :
              #   outputs=res["outputs"]

           else:
              time.sleep(2)
        return vreport


   def analyseToolResponse(self,txt):
       #fix me
        x = txt.split("\n")
        r=""
        g=0 
        for el in x:
           if el.strip() == "{" :
               g=1
           if g==1:
              r+=el

           if el.strip() == "}" :
               g=0
 
        return r 


   def getFileWithAPI(self,jobid, fn):
        #not working
        #not jtes retrieving file ??
        jtesclient= self.jtesclient()

        api_result = jtesclient.getFile(jobid,fn)
      

        return api_result


   def statusTaskWithAPI(self,jobid):
        #wait (Sync)

       
        jtesclient= self.jtesclient()

        api_result = jtesclient.taskStatus(jobid)
      

        return api_result



   def processWithAPI(self,dsl,vreport):
        #wait (Sync)
        #get result
        
        jtesclient= self.jtesclient()


        ipathList=list()
        #ifilenameList=list()
        #for ds in dsl:
        #  ipath=ds.path
        #  ifilenameList.append(ipath)
        #  ifilename=os.path.basename(ipath)
        #  ipathList.append(ipath)
#        cmd="python transcribe_argparse.py -d    %s --verbose" % (ifilename)
#        cmd="wc -m -l   %s | cut -f 2 -d ' ' " % (ifilename)
        #cmd= self.cmdTemplate  % (ifilenameList)
         #FIXME : see base.py
        workdir=os.path.dirname(settings.DEFAULT_JTES_WORDIR)+"/" 
        contextmap=dict()
        for pk in self.paramd.keys():
            v=None
            el=self.paramd[pk]
            if   isinstance(el , Dataset)==True:
               v=el.path
               ipath=v
               workdir=os.path.dirname(ipath)
               ipathList.append(ipath) 
            else:
               v=el
            contextmap[pk]=v
         
       
        #contextmap["para_datamodel"]="/data/model/pegase.jar"
        #contextmap["tab"]="/tmp/excl.xls"
        contextmap=self.tengine.removePrefix(contextmap)

        

        cmd=self.tengine.renderTemplate(self.cmdTemplate,contextmap)
       # print("==============")
       # print("==============$$$$$$$$$$$$$$$$$$$$$$")
        print(cmd)
        #print("=====================")
        #cmd= self.cmdTemplate  % (ipathList)
#       script="/work/data/transcribe_argparse.py"
        script=None
       
        image=self.image
#        workdir="/work/data/"
       
        taskstr=jtesclient.jsontask("task1",ipathList, script,cmd,image, workdir)
        
        vreport["task"]=taskstr

        api_result = jtesclient.submitTaskFromJson(taskstr)
        #create jtask (+command line +docker)
        #submitTask 
        vreport["res"]=api_result
        return vreport

   def defineParamFromRequest(self,request):
      paramd= dict()
      for key, value in request.POST.items():
            print('field: %s:%s' % (key, value) ) 
            if  key.startswith("para_"):
               paramd[key]=value
            elif key.startswith("data_"):
               ds = Dataset(filename=os.path.basename(value), store_date=datetime.date.today(),path=value)
               paramd[key]=ds

      return paramd

   def createUploadSubDir(self,rootd):
      today=datetime.date.today()
      #key=today.strftime("%d-%m-%y-%H:%M::%s")#%H:%M:%S.%f
      num = random.randint(0, 1000)
      fx=today.strftime("%H-%M-%S-%s")+"-"+str(num)
      folder=rootd+'/upload/'+today.isoformat()+"/"
      sfold=folder+""+fx+""

      if not os.path.exists(rootd):
         os.makedirs(rootd)
      if not os.path.exists(folder):
         os.makedirs(folder)
      if not os.path.exists(sfold):
         os.makedirs(sfold)

      return  sfold


   def formatNName(self,fname,pre="",suf=""):
   
     basen=os.path.basename(fname)
     nfname=os.path.splitext(basen)
     nnf=nfname[0]
     nnf=nnf.replace(' ', '-')
     nnf = "".join(i for i in nnf if i not in "\/:*?<>|")
     ext=""
     if len(nfname)>1:
        ext=nfname[1]
     dst=pre+""+nnf+suf+ext
     return dst



   def uploadInputFiles(self,request):


      
      rootd=settings.MEDIA_URL

      sfold=self.createUploadSubDir(rootd)


      if request.method == 'POST' and request.FILES:
 #         dsl=list()
          dsm=dict()
          for fk in request.FILES.keys() :
            #print("===>"+key)
            file1=request.FILES[fk]
            fin=file1.field_name
            #print("===>  "+fin)
            print(dir(file1))
            
            if fin.startswith("data_"):
             
              fs = FileSystemStorage(location=rootd) #defaults to   MEDIA_ROOT  
              filename = fs.save(file1.name, file1)
              file_url_raw = fs.url(filename)
              src=rootd+"/"+filename

              dst=sfold+"/"+self.formatNName(file1.name)

              move(src, dst)
              #

              ds = Dataset(filename=file1.name, store_date=datetime.date.today(),path=dst)
#              dsl.append(ds)
              dsm[fin]=ds
              #ds.save()
              #vreport=self.processDs(ds)

              return dsm


      else:
         return None



class JWTAuthValidatorUpload(JWTAuthUpload):

   def  initialize(self):
       # print("INITIALIAZE!!!!!!!!!!!!!!!!!!!!!!!!")
        #self.image= "fjrmore/parsetab"
        self.image= "fjrmore/parsetab"
      #  self.cmdTemplate= "ls -l ; bash /parsetab/testparse.sh   %s   "
     #PARAM="  example/tab/lipmuscl.xlsx example/jpa_generated_test2.jar vstatus.json"

     #TODO : use django template here with {{DATA}} and {{PARA}} or $DATA1 , $PARAM1
        self.cmdTemplate= "bash /parsetab/parsetab.sh   ${tab}   ${datamodel}   vstatus.json "
      #  self.cmdTemplate= "echo %s && ls / "

    

   def inputform(self,request,context):

    form = ValidatorUploadDocumentForm()
    if request.method == 'POST':
        form = ValidatorUploadDocumentForm(request.POST, request.FILES)   
    return form








