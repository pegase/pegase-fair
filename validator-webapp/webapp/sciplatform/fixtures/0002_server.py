from dynamic_fixtures.fixtures import BaseFixture

from django_keycloak.models import Server
from django.conf import settings
import os

 

class Fixture(BaseFixture):


    def load(self):
        inteurl=settings.KEYCLOAK_CUSTOM_INTERNAL_URL
        idurl=settings.IDENTITY_SCHEMA_DOMAIN
        
        print("===================" ) 
        print("===================" ) 
        print("=======Server::Fixture::load============" ) 
        print("===================" ) 
        print("========= url:%s==========" %(idurl) ) 
        print("=========internal_url:%s==========" %(inteurl) ) 
#        Server.objects.get_or_create(
#            url='https://identity.localhost.yarf.nl',
#            internal_url=inteurl
#        )
        Server.objects.get_or_create(
            url=idurl,
            internal_url=inteurl
        )



