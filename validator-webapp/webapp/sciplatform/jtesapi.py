#Author
#FM  09/2019
from __future__ import unicode_literals

import os
import json
import requests

from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone

import django_keycloak.services.oidc_profile
import django_keycloak.services.remote_client

class JTESClient():

    def __init__(self,request,url,auth=0): 
        self.request = request
        self.jtesurl=url
        self.v1="/tes/v1"
        self.isauth =bool(auth)
        self.debug_mode=1

    def api_access_token(self): 
        if not hasattr(self.request.user, 'oidc_profile'):
            return None
        oidc_profile = self.request.user.oidc_profile


        remote_client = oidc_profile.realm.remote_clients.get(
            name='micro-api')
        tk = django_keycloak.services.remote_client.get_active_remote_client_token(
            oidc_profile=oidc_profile, remote_client=remote_client)
        return tk



    def serviceinfo(self):
 
        api_access_token=self.defineToken()
        headers =self.defineAuthHeader(api_access_token)
 
 
        

        result = requests.get(
            self.jtesurl+'/service-info',
            verify=False,
            headers=headers
        )
        #parsed = json.loads(result.text)
        ret=json.dumps(result.json(), indent=4, sort_keys=True) 

        return   ret 




    def submitTaskFromFile(self,jsonFile):
        f = open(jsonFile, "r") 
        txt=f.read() 
        return self.submitTaskFromJson(txt)

    def submitTaskFromJson(self,txt):
        #data=json.loads(txt)
        data=txt
        return self.submitTask(data)
 

 #   def submitTaskFromDict(self,d):
 #       data=json.dumps(d)
 #       return self.submitTask(data)
 
    def defineToken(self):
        api_access_token=None
        if self.isauth ==True:
          api_access_token = self.api_access_token()
          if api_access_token is None:
            return None
        return api_access_token


    def defineAuthHeader(self,token):
        headers={}
        if self.isauth ==True:
          headers['Authorization']='Bearer {}'.format(token)
        return headers


    def submitTask(self,data):
        print("submitTask1.1\n%s" %(str(data)))

        api_access_token=self.defineToken()
        headers =self.defineAuthHeader(api_access_token)
 
        headers['Content-type']='application/json'

 
        result = requests.post(
            url =self.jtesurl+self.v1+'/tasks',
            data =data,
            verify=False,
            headers=headers
             
        )
        #parsed = json.loads(result.text)
        #r.text 
        #ret=json.dumps(result.json(), indent=4, sort_keys=True) 
        
        self.log_response(result)
        ret=result.json()
        return   ret 

    def log_response(self,respon):
        if self.debug_mode==1 :
           print("---------------------------------")
           if respon is None:
             print("----response)=None-----------------------------")
           else: 
             print("status_code:%s" %(respon.status_code))
             print("text:%s"%(respon.text))
           print("---------------------------------")


    def taskStatus(self,jobid):
        api_access_token=self.defineToken()
        headers =self.defineAuthHeader(api_access_token)
 
        headers['Content-type']='application/json'

        result = requests.get(
            url =self.jtesurl+self.v1+'/tasks/'+jobid,
            verify=False,
            headers=headers
            
        )
        
        #ret=json.dumps(result.json(), indent=4, sort_keys=True) 
        ret=result.json()
        return   ret 


 
    def getFile(self,jobid, filename):
        api_access_token=self.defineToken()
        headers =self.defineAuthHeader(api_access_token)
 
        headers['Content-type']='application/json'

        result = requests.get(
            url =self.jtesurl+self.v1+'/get-file/'+jobid+'/'+filename+'/',
            verify=False,
            stream=True, 
            headers=headers
            
        )
        
        #ret=json.dumps(result.json(), indent=4, sort_keys=True) 
        ret=result.text
        return   ret 

 



    def jsontask(self,taskname,inpFList,script,cmd, image, outd):

        jin1=""
        if inpFList is not None:
          jil=list() 
          for in1 in inpFList:

             
             ifilename=os.path.basename(in1)
             ji=""" 
             {
                         "name" : "%s",
                         "description" : null,
                         "url" : "file:///%s",
                         "path" : "%s",
                         "type" : "FILE",
                         "content" : null
             }
             """ %(ifilename,in1,in1)
             jil.append(ji)


          jin1=",".join(jil)



        jscript=""
        sep1=""
        if script is not None:
            sep1=","
            jscript=""" 
            {
            "name" : "script",
            "description" : null,
            "url" : "file:///%s",
            "path" : "%s",
            "type" : "FILE",
            "content" : null
            }
            """ %(script,script)

        json="""{
    "name" : "%s",
    "description" : null,
    "inputs" : [ %s %s %s ],
    "outputs" : [ {
        "name" : "output1",
        "description" : null,
        "url" : "file:///%s",
        "path" : "%s",
        "type" : "DIRECTORY"
    } ],
    "resources" : {
        "cpu_cores" : null,
        "preemptible" : false,
        "ram_gb" : null,
        "disk_gb" : null,
        "zones" : null
    },
    "executors" : [ {
        "image" : "%s",
        "command" : [ "/bin/sh", "-c", "%s" ],
        "workdir" : "%s",
        "stdin" : null,
        "stdout" : "%s",
        "stderr" : "%s",
        "env" : {
            "HOME" : "%s",
            "TMPDIR" : "%s"
        }
    } ],
    "volumes" : null,
    "tags" : {
        "job_id" : null,
        "tool_name" : null,
        "workflow_id" : null
    },
    "creation_time" : null,
    "id" : null,
    "state" : null,
    "logs" : null
    }
        """ %(taskname,jin1,sep1,
                    jscript,outd,outd,image,cmd,
                    outd,    outd+"std.out",outd+"std.err",outd,outd+"/tmp")


        return json




