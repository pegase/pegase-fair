from django import forms

#forms.ModelForm => form in the DB


class UploadDocumentForm(forms.Form):
    file1 = forms.FileField( label='Select a file ', help_text='max. 42 megabytes')
    #image = forms.ImageField()

class ValidatorUploadDocumentForm(forms.Form):
    data_tab = forms.FileField( label='Select a file (excel/libre office sheet) max. 42 Mb', help_text='')
    data_tab.widget.attrs.update({'class' : 'filecl1'})
    #image = forms.ImageField()
    #choices = [('1', 'First'), ('2', 'Second')]
    #datamodel0=forms.ChoiceField(widget=forms.RadioSelect, choices=choices)
    choices1 = [( '/media/upload/reference/model.gen.pegase_v1.jar', 'PEGASE model V0.1'), ( '/media/upload/reference/jpa_generated_test2.jar', 'TEST model')]
    data_datamodel=forms.ChoiceField(label='data model', widget=forms.Select, choices=choices1)
    data_datamodel.widget.attrs.update({'class' : 'selectcl1'})
    

   
