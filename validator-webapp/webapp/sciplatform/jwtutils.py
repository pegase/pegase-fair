#Author
#FM  09/2019
from __future__ import unicode_literals

import json
import requests

from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone

import django_keycloak.services.oidc_profile
import django_keycloak.services.remote_client

class CustomJWTManager():

    def __init__(self,request): 
        self.request = request

  

    #    template_name = 'myapp/permission.html'
    ##    permission_required = 'some-permission'
    ##before in KC 3.X, was in user/"clientRoles" : { "resource-provider" : [ "some-permission" ],
    ######FM+ KC 5.X
    # we map KC role on django permission for 
    #simplicity and compatibility with spring micro services
    ###in MyView(PermissionRequiredMixin, TemplateView) : permission_required = 'ADMIN' 
    
    ## backends.py
    ###see get_all_permissions
 
 

    def confRoleListFromAccessToken(self):
        ############original package only for KC <=3.X (entitlement API
        #permissions=self.request.user.get_all_permissions(),
        ############for more recent KC (tested on 5.X ) : 
        #updated in package django-keycloak-dev-cust-fm
        # see backends.py
        rolelist=list()      
        rl=self.request.user.get_all_permissions() # return a set not direcly serializable in  json !!!
        for el in rl:
           rolelist.append(el)
        self._confRoleListFromAccessToken_impl(rolelist)
        return rolelist


    def _confRoleListFromAccessToken_impl(self,rl):
         if rl is None:
            rl=[] 
         self.request.session['rolelist'] = rl

    def confSessionAccessToken(self,tk):
         print("define request.session['latoken']:"+str(tk))
         self.request.session['latoken'] = str(tk)

    def _refresh_param(self):
         #return "NO"
         #return "WHEN_EXPIRE"
         return "FORCE"          

    def  refresh_access_token(self,oidc_profile):
        tk=None
        if self._refresh_param()=="FORCE":
           tk=self._force_refresh_access_token(oidc_profile)
        elif self._refresh_param()=="WHEN_EXPIRE":
           tk=django_keycloak.services.oidc_profile.get_active_access_token(oidc_profile=oidc_profile)
        else :
           tk=oidc_profile.access_token #no refresh
        return tk

    def _force_refresh_access_token(self,oidc_profile):
     
        initiate_time = timezone.now()

        token_response = oidc_profile.realm.client.openid_api_client\
            .refresh_token(refresh_token=oidc_profile.refresh_token)

        oidc_profile = django_keycloak.services.oidc_profile.update_tokens(token_model=oidc_profile,
                                     token_response=token_response,
                                     initiate_time=initiate_time)

        return oidc_profile.access_token

 

    def userClient(self):
        oidc_profile = self.request.user.oidc_profile
        client = oidc_profile.realm.client
        return client

    def userDecodedJWTAccessToken(self):
        oidc_profile = self.request.user.oidc_profile
        jwt = django_keycloak.services.oidc_profile.get_decoded_jwt(oidc_profile=oidc_profile)
        return jwt

    def userEncodedJWTAccessToken(self):
     
        oidc_profile = self.request.user.oidc_profile
        if oidc_profile is not None:  
          ejwt=self.refresh_access_token(oidc_profile)
          # self.confSessionAccessToken(tk)
          return ejwt
        return None


    def userProfile(self):
        oidc_profile = self.request.user.oidc_profile
        return oidc_profile

    def checkSessionIframe(self):
        return self.request.realm.well_known_oidc['check_session_iframe']


#    def get_decoded_jwt(self):
#        if not hasattr(self.request.user, 'oidc_profile'):
#            return None
#
#        oidc_profile = self.userProfile()
#        client = self.userClient()
#        tk=self.userAccessToken()
#
#        return client.openid_api_client.decode_token(
#            token=tk,
#            key=client.realm.certs,
#            algorithms=client.openid_api_client.well_known[
#                'id_token_signing_alg_values_supported']
#        )











