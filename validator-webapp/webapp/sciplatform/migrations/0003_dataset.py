# Generated by Django 2.0.13 on 2019-10-07 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sciplatform', '0002_auto_20180322_2059'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dataset',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('filename', models.CharField(max_length=120, verbose_name='filename')),
                ('path', models.CharField(max_length=120, verbose_name='path')),
                ('store_date', models.DateTimeField(verbose_name='date')),
                ('description', models.TextField(blank=True)),
            ],
        ),
    ]
