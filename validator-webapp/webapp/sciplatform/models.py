# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Dataset(models.Model):
     filename = models.CharField('filename', max_length=120)
     path = models.CharField('path', max_length=120)
     store_date = models.DateTimeField('date')
     description = models.TextField(blank=True)
