from __future__ import unicode_literals

########
import json
import os
import datetime
import time
import logging
import requests

from django.contrib.auth import get_user_model
 
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views import generic
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.http import HttpResponse, JsonResponse
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage

 

IS_LEGACY_TX = int(os.environ.get('IS_LEGACY_TX', '-1'))
if IS_LEGACY_TX==1:
   from toxsign.cohorts.models import Cohort


#################KC+

 
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    PermissionRequiredMixin
)
from django.views.generic.base import TemplateView
from jose.exceptions import JWTError

import django_keycloak.services.oidc_profile
import django_keycloak.services.remote_client


from sciplatform.jwtutils import CustomJWTManager
from sciplatform.jtesapi import JTESClient
from sciplatform import context_processors
from sciplatform.utils import tojson

logger = logging.getLogger(__name__)



 
 

class Simple(TemplateView):
#    template_name = 'myapp/home.html'
    def any(self):
        print("simple:TemplateView:no auth") 



class JTESExample(LoginRequiredMixin, TemplateView):
 


    def get_context_data(self, **kwargs):

        #kwargs = super().get_context_data(**kwargs)
        

        jmanager=   CustomJWTManager(self.request)
        api_result ="--undef--"
        latoken=jmanager.userEncodedJWTAccessToken()
        try:
            jwt = jmanager.userDecodedJWTAccessToken()
        except JWTError:
            jwt = None
        rolelist= jmanager.confRoleListFromAccessToken()



     
        atoken=jmanager.userEncodedJWTAccessToken()
        jwtstr= tojson(jwt)


        context = super(JTESExample, self).get_context_data(
               permissions=rolelist,
               op_location=jmanager.checkSessionIframe(), 
               **kwargs)


 
        jtesclient = CustomJWTManager(self.request)

 
        debugstr=str(kwargs)+"settings:"+str(dir(settings))       
        jtesurl=settings.JTESURL
        jtesclient= JTESClient(self.request, jtesurl)
        api_result = jtesclient.serviceinfo()




        
        context['access_token'] = atoken
        context['api_result'] = api_result
        context['jwt'] = jwtstr
        context['rolelist_str'] = tojson(rolelist)
        context['debugstr'] = debugstr

        return context 


#        return super(Secured, self).get_context_data(
#            permissions=rolelist, 
#            access_token=atoken,
#            api_result=api_result,
#            jwt=jwtstr,
#            op_location=jmanager.checkSessionIframe(),
#            **kwargs
#        )

    






 
def HomeView(request):
    if request.user.is_authenticated :
        if IS_LEGACY_TX==1:
           all_cohorts = Cohort.objects.all().order_by('id')
           context = {'all_cohorts':all_cohorts}
        else:
           context = {'all_cohorts':[]}
        return render(request, 'pages/home.html',context)
    else:
            x = "Not OK"
            return HttpResponseRedirect(reverse('account_login'))










