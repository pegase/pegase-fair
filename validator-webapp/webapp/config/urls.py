import os
from django.conf import settings
from django.conf.urls import url
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views
from . import views
from sciplatform.sciviews import JWTAuth, JWTAuthUpload,JWTAuthValidatorUpload

'''
urlpatterns = [

    url(r'^$', views.Simple.as_view(template_name="sciplatform/kchome.html"), name='index'),
    path("home", views.Simple.as_view(template_name="sciplatform/kchome.html"), name="home"),
    url(r'^secured$',views.Secured.as_view(template_name="sciplatform/kcsecured.html"), name='secured'),
    url(r'^permission$', views.Permission.as_view(template_name="sciplatform/kcpermission2.html"), name='permission'),
    url(r'^keycloak/', include('django_keycloak.urls')),
    url(r'^admin/', admin.site.urls),
 ]
'''
 
urlpatterns =   [
###############KC+
    url(r'^$', views.Simple.as_view(template_name="sciplatform/kchome.html"), name='index'),
    path("home", views.Simple.as_view(template_name="sciplatform/kchome.html"), name="home"),
    path("home-old", views.HomeView, name="home-old"),
    #path("", views.HomeView, name="home"),
    url(r'^secured$',views.JTESExample.as_view(template_name="sciplatform/kcsecured.html"), name='secured'),
    url(r'^permission$', JWTAuth.as_view(template_name="sciplatform/kcpermission2.html"), name='permission'),
    url(r'^keycloak/', include('django_keycloak.urls')),
    url(r'^admin/', admin.site.urls),
###############KC-
#test JTES
    url(r'^upload_form$',  JWTAuthUpload.as_view(template_name="sciplatform/upload.html"), name='upload_form'),
    url(r'^upload_proc$',  JWTAuthUpload.as_view(template_name="sciplatform/upload.html"), name='upload_proc'),

###############PEGASEREPO
    url(r'^validator_form$',  JWTAuthValidatorUpload.as_view(template_name="sciplatform/validator.html"), name='validator_form'),
    url(r'^validator_proc$',  JWTAuthValidatorUpload.as_view(template_name="sciplatform/validator.html"), name='validator_proc'),


 
    # Your stuff: custom urls includes go here
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

##############################
IS_LEGACY_TX = int(os.environ.get('IS_LEGACY_TX', '-1'))
if IS_LEGACY_TX==1:
   urlpatternsT =   [
 

    #path("ajax_calls/search/", views.autocompleteModel, name="search"),
    #path("search_results/<str:query>", views.search, name="results"),
    #path("index/", views.index, name="index"),
    path(
        "about/", TemplateView.as_view(template_name="pages/about.html"), name="about"
    ),
    path(
        "tutorial/", TemplateView.as_view(template_name="pages/tutorial.html"), name="tutorial"
    ),
    path(
        "statistics/", TemplateView.as_view(template_name="pages/statistics.html"), name="statistics"
    ),
    path(
        "download/", TemplateView.as_view(template_name="pages/download.html"), name="download"
    ),
    path(
        "help/", TemplateView.as_view(template_name="pages/help.html"), name="help"
    ),
    #path(
    #    "graphs/", views.graph_data, name="graph"
    #),
    #path("unauthorized", views.render_403, name="unauthorized"),
    
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path("groups/", include("toxsign.groups.urls", namespace="groups")),
    path("users/", include("toxsign.users.urls", namespace="users")),
    path("projects/", include("toxsign.projects.urls", namespace="projects")),
    path("studies/", include("toxsign.studies.urls", namespace="studies")),
    path("assays/", include("toxsign.assays.urls", namespace="assays")),
    path("signatures/", include("toxsign.signatures.urls", namespace="signatures")),
    path("tools/", include("toxsign.tools.urls", namespace="tools")),
    path("jobs/", include("toxsign.jobs.urls", namespace="jobs")),
    path("cohorts/", include("toxsign.cohorts.urls", namespace="cohorts")),
    path("databases/", include("toxsign.databases.urls", namespace="databases")),
    path("accounts/", include("allauth.urls")),
    path("ontologies/", include("toxsign.ontologies.urls", namespace="ontologies"))
   ] 
else:
   urlpatternsT =   []    


##############################

urlpatterns = urlpatterns + urlpatternsT


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns

 


