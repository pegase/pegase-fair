#!/usr/bin/env sh
set -e


 toxsignmigrate() {


 echo "@@@@@@@@@@@@@MIGRATE:1.6.2"
  python manage.py migrate signatures
 echo "@@@@@@@@@@@@@MIGRATE:1.6.3"
  python manage.py migrate socialaccount
  echo "@@@@@@@@@@@@@MIGRATE:1.6.4"
  python manage.py migrate studies
  echo "@@@@@@@@@@@@@MIGRATE:1.6.5"
  python manage.py migrate  tools 
 echo "@@@@@@@@@@@@@MIGRATE:1.7.0"
   python manage.py migrate  assays
 echo "@@@@@@@@@@@@@MIGRATE:1.7.1"
   python manage.py migrate  cohorts

 echo "@@@@@@@@@@@@@MIGRATE:1.7.3"
   python manage.py migrate  databases  

  echo "@@@@@@@@@@@@@MIGRATE:1.8.1"
   python manage.py migrate groups
  echo "@@@@@@@@@@@@@MIGRATE:1.8.2"
   python manage.py migrate guardian
  echo "@@@@@@@@@@@@@MIGRATE:1.8.3"
  python manage.py migrate ontologies
  echo "@@@@@@@@@@@@@MIGRATE:1.8.4"

  python manage.py migrate projects
  echo "@@@@@@@@@@@@@MIGRATE:1.8.5"
  python manage.py migrate  jobs  
  echo "@@@@@@@@@@@@@MIGRATE:1.8.6"



}


scimake(){

 #python manage.py makemigrations sciplatform.apps.MyAppConfig
echo "SCIMAKE START"
 python manage.py makemigrations sciplatform
echo "SCIMAKE DONE"


}


scimigrate(){

  echo "@@@@@@@@@@@@@MIGRATE:1.9.0.1"
 python manage.py migrate sciplatform
#  echo "@@@@@@@@@@@@@MIGRATE:1.9.0.2"
#    python manage.py  showmigrations  

   echo "@@@@@@@@@@@@@MIGRATE:1.9.1"
   python manage.py load_dynamic_fixtures sciplatform
 #  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" 
 #  echo "@@@@@@@@@@@@@MIGRATE:1.9.2" 
   #python manage.py migrate  guardian 
   #python manage.py migrate  projects 
   #python manage.py migrate  socialaccount

#   echo "@@@@@@@@@@@@@MIGRATE:1.9.3"
#    python manage.py load_dynamic_fixtures django-keycloak


}

echo "entrypoint-dev------1------"
PYTHON_VERSION_INS="3.6"
#pip install -e ./../django-keycloak/

#pip install -e ./../python-keycloak-client/ || true
#echo "entrypoint-dev------3------"
#pip install -e ./../django-dynamic-fixtures/ || true
#echo "entrypoint-dev------4------"

pip uninstall -y django-keycloak
pip install -e ./resources/django-keycloak-cust-fm/

##################FROM TD +
set -o errexit
set -o pipefail
set -o nounset

echo "@@@@@@@@@@@@@@@@@@@@=1--IS_LEGACY_TX:$IS_LEGACY_TX"

if [ -z "${USE_POSGRES_CUST}" ]; then
 echo "-----------USE_POSGRES_CUST not defined : initializing it to 0---------"
  USE_POSGRES_CUST="0" 
else
 echo "-----------USE_POSGRES_CUST defined---------"
fi


if [ "$USE_POSGRES_CUST" == "1" ]; then

echo "-----------going to use posgres---------"
# N.B. If only .env files supported variable expansion...
export CELERY_BROKER_URL="${REDIS_URL}"


echo "@@@@@@@@@@@@@@@@@@@@=2"

if [ -z "${POSTGRES_USER}" ]; then
    base_postgres_image_default_user='postgres'
    export POSTGRES_USER="${base_postgres_image_default_user}"
fi
export DATABASE_URL="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}"

postgres_ready() {
python << END
import sys

import psycopg2

try:
    psycopg2.connect(
        dbname="${POSTGRES_DB}",
        user="${POSTGRES_USER}",
        password="${POSTGRES_PASSWORD}",
        host="${POSTGRES_HOST}",
        port="${POSTGRES_PORT}",
    )
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)

END
}
until postgres_ready; do
  >&2 echo 'Waiting for PostgreSQL to become available...'
  sleep 1
done
>&2 echo 'PostgreSQL is available'

else

  echo "-----------not going to use posgres---------"

fi
##################FROM TD -




echo "@@@@@@@@@@@@@@@@@@@@=3"

################rm -f sciplatform/db.sqlite3
echo "fixme : rm -f sciplatform/db.sqlite3"
rm -f sciplatform/db.sqlite3
##################
if [ -f sciplatform/db.sqlite3 ]; then
    echo "entrypoint-dev------2.1------"
    echo "entrypoint-dev------sciplatform/db.sqlite3 :exists------"
    echo "Application already initialized."
else
    echo "entrypoint-dev------2.2------"
    #sleep 5
    echo "Initializing application"
    echo "Initializing application: python manage.py showmigrations"

    echo "@@@@@@@@@@@@@MIGRATE:1.0"

#    python manage.py  showmigrations  
    # Run migrations
    echo "Initializing application: python manage.py migrate"
#Apply all migrations: admin, auth, contenttypes, django_keycloak, guardian, sciplatform, sessions, sites, users
 echo "@@@@@@@@@@@@@MIGRATE:1.1"
    python manage.py migrate users
 echo "@@@@@@@@@@@@@MIGRATE:1.2"
    python manage.py migrate auth


 echo "@@@@@@@@@@@@@MIGRATE:1.3"
   python manage.py migrate django_keycloak 
 echo "@@@@@@@@@@@@@MIGRATE:1.4"
   python manage.py migrate admin 

 echo "@@@@@@@@@@@@@MIGRATE:1.5"
   python manage.py migrate account

 echo "@@@@@@@@@@@@@MIGRATE:1.6.0"
   python manage.py migrate sessions

 echo "@@@@@@@@@@@@@MIGRATE:1.6.1.1"
  python manage.py migrate sites

 echo "@@@@@@@@@@@@@MIGRATE:1.6.1.2"
   python manage.py migrate   contenttypes

  if [ "$IS_LEGACY_TX" -eq "1" ]; then
  
	 
	  toxsignmigrate
  fi  
  #scimake
  scimigrate
 



fi

if grep -q Yarf /usr/local/lib/python${PYTHON_VERSION_INS}/site-packages/certifi/cacert.pem
    then
        echo "entrypoint-dev------3.1------"
        echo "CA already added"
        
    else
        echo "entrypoint-dev------3.2------"
        echo "Add CA to trusted pool"
        echo "\n\n# Yarf" >> /usr/local/lib/python${PYTHON_VERSION_INS}/site-packages/certifi/cacert.pem
        cat /usr/src/ca.pem >> /usr/local/lib/python${PYTHON_VERSION_INS}/site-packages/certifi/cacert.pem
fi

###keycloal client with debug
mv  /usr/local/lib/python3.6/site-packages/keycloak/client.py   /usr/local/lib/python3.6/site-packages/keycloak/client.py.sav
cp ./resources/client.py /usr/local/lib/python3.6/site-packages/keycloak/client.py
cp ./resources/jwt.py /usr/local/lib/python3.6/site-packages/jose/jwt.py 
#exec "$@"
sh "$@"



 
