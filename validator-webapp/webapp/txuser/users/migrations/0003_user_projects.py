# Generated by Django 2.0.13 on 2019-05-23 13:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0001_initial'),
        ('users', '0002_auto_20190523_0800'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='projects',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='projects', to='projects.Project'),
        ),
    ]
