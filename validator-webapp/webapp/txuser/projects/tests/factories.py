from factory import DjangoModelFactory, Faker, SubFactory
from txuser.projects.models import Project
from txuser.users.tests.factories import UserFactory

class ProjectFactory(DjangoModelFactory):

    name = Faker("name")
    created_by = SubFactory(UserFactory)
    status = "PRIVATE"
    description = Faker("text")

    class Meta:
        model = Project
