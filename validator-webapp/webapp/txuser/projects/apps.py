from django.apps import AppConfig


class ProjectsAppConfig(AppConfig):

    name = "txuser.projects"
    verbose_name = "Projects"
