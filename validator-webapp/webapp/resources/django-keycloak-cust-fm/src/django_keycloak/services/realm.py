from keycloak.realm import KeycloakRealm

import os

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

#FM custom
def define_custom_server_url(realm, nodefval=False):

    if nodefval == True:
      inteurl=None
    else:
      inteurl=realm.server.url

    if realm.server.internal_url: 
        inteurl=realm.server.internal_url
#    else:
# 
#       KEYCLOAK_CUSTOM_INTERNAL_URL=os.environ.get('KEYCLOAK_CUSTOM_INTERNAL_URL', None)
#       if KEYCLOAK_CUSTOM_INTERNAL_URL is not None:
# 
#         inteurl=KEYCLOAK_CUSTOM_INTERNAL_URL
    return inteurl

    
def get_realm_api_client(realm):
    """
    :param django_keycloak.models.Realm realm:
    :return keycloak.realm.Realm:
    """
    print("+++realm:get_realm_api_client")
    print("====@@@@@@@@@@@@realm.py:get_realm_api_client")
    headers = {}
    server_url = realm.server.url
    print("====@@@@@@@@@@@@ realm.py  realm.server.url1: %s" %(realm.server.url))
    print("====@@@@@@@@@@@@ realm.py  realm.server.internal_url:%s" %(realm.server.internal_url) ) 
    ##FM custom
    inteurl=define_custom_server_url(realm,True)

  
    if inteurl is not None:

        # An internal URL is configured. We add some additional settings to let
        # Keycloak think that we access it using the server_url.
        server_url = inteurl
        parsed_url = urlparse(realm.server.url)
        headers['Host'] = parsed_url.netloc

        if parsed_url.scheme == 'https':
            headers['X-Forwarded-Proto'] = 'https'
    print("====@@@@@@@@@@@@ realm.py REALM server_url2: %s" %(server_url))
    return KeycloakRealm(server_url=server_url, realm_name=realm.name,
                         headers=headers)


def refresh_certs(realm):
    print("+++realm:refresh_certs")
    """
    :param django_keycloak.models.Realm realm:
    :rtype django_keycloak.models.Realm
    """
    realm.certs = realm.client.openid_api_client.certs()
    realm.save(update_fields=['_certs'])
    return realm


def refresh_well_known_oidc(realm):
    print("+++realm:refresh_well_known_oidc")
    """
    Refresh Open ID Connect .well-known

    :param django_keycloak.models.Realm realm:
    :rtype django_keycloak.models.Realm
    """
    #fm custom
    #server_url = realm.server.internal_url or realm.server.url
    server_url =define_custom_server_url(realm)
    # While fetching the well_known we should not use the prepared URL
    openid_api_client = KeycloakRealm(
        server_url=server_url,
        realm_name=realm.name
    ).open_id_connect(client_id='', client_secret='')

    realm.well_known_oidc = openid_api_client.well_known.contents
    realm.save(update_fields=['_well_known_oidc'])
    return realm


def get_issuer(realm):
    print("+++realm:get_issuer")
    """
    Get correct issuer to validate the JWT against. If an internal URL is
    configured for the server it will be replaced with the public one.

    :param django_keycloak.models.Realm realm:
    :return: issuer
    :rtype: str
    """
    issuer = realm.well_known_oidc['issuer']
    cinternal_url =define_custom_server_url(realm,True)
    if  cinternal_url:
        return issuer.replace(cinternal_url, realm.server.url, 1)

    #if realm.server.internal_url:
    #    return issuer.replace(realm.server.internal_url, realm.server.url, 1)
    return issuer


