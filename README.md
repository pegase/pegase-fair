# PEGASE-FAIR

**PEGASE-FAIR : Scientific Data structuration  using UML and Model Driven Architecture**




The following components are available:

1. uml2jpa 

   UML to JAVA  code generator 

   convert a class diagram, from UML XMI (generated with modelio)

   to   JAVA classes with JPA and custom annotations. The classes are packaged in a jar that can be reused in further steps.

2. jpa2rdf

   generate RDF/XML  or RDF/turle files  from tabular data files that match a Data model  (UML class diagram, converted as JAVA JPA  classes and available a a JAR)

3. jpa2tabvalidator

   validate the tabular data files using the Data model (as a JAR)

4. validator-webapp

    web app to validate data files.  .

5. data2dataverse  

    module to export results to a  Dataverse instance.

6. Diagram_UML_data_template 

   example large UML class diagram as developped for the INRAE PEGASE UML

   ​

   ​

   ​



















