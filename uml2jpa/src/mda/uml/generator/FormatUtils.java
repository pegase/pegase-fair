package mda.uml.generator;

import java.util.List;

import javax.validation.constraints.Size;

import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.internal.impl.PropertyImpl;

import uml2rdf.utils.Info;

import javax.measure.*;


public class FormatUtils {
/*
 has unit: g/100g
min>0
nullable: NA, LD, LQ
------------------------------
has unit: µm²
-------------------------------
has unit: mg
min>0
-------------------------------


 */
 
	public static String testformat(String v){
		
		return "TEST_"+v+"_TEST";
	}
	
	public static String displayClass(Object o){
		return o.getClass().getName() ;
	}
	
	
	public static void main(String[] args) {
		/*
		 //String v="max = 10";
		 String v="<10";
		 System.out.println(extractAfterVal(v, "<"));;
	//	String a="has unit: µmole/ml\nmin>0\nnullable: NA, LD, LQ";
	//	 String v=anyAnnotSplit(a);
	//	 System.out.println(v);
				*/
		  
				String s="desc: Seuil canaux\n" + 
						"# desc2:  format: nombre entier, ne pas spécifier d'unité\n" + 
						"# desc3: ex:... ou NA\n";
				s+="ordered(rindex=1)";
				System.out.println(s);
				String r=anyDescSplit(s);
				
				System.out.println("==");
				System.out.println(r);
				
                 r=anyAnnotSplit(s);
				 
				System.out.println(r);
			 
	}
	
	//
	//
	/*
    <ownedAttribute xmi:id="_s_SWeXfOEemKR73gi6jJqA" name="date" visibility="public" isUnique="false">
     <eAnnotations xmi:id="_s_SWenfOEemKR73gi6jJqA" source="Objing"/>
     <ownedComment xmi:id="_s_SWe3fOEemKR73gi6jJqA">
       <body>Date de l'analyse</body>
     </ownedComment>
   */
	

	public static String describe(Object o){
	
		return o.getClass().getName();
	}
	
	public static String anyAnnot(String v, DataSize sz){
		
		String s=null;
		// @AssertTrue
		String vv=v.trim().toLowerCase().replaceAll("\\s", "");
		if(vv.contains("nullable")){		
			s= "@Null";
		}
		if(vv.contains("unit:")){
			String unit=extractUnitVal(vv, "unit:");
			 
			s= "@CustomUnit(value=\""+unit+"\" )";
		}

		
		if(vv.contains(">") || vv.contains("min")){		
			s=extractAfterVal(vv, ">");
			if(s==null){
				s=extractAfterVal(vv, "min=");
			}
			
			if(s!=null){
		    sz.min=s;
		    sz.valid=true;
		    System.out.println("sz.valid=true");
			}
		    return null;
		    
		}
		if(vv.contains("<") ||   vv.contains("max")){
			s=extractAfterVal(vv, "<");
			if(s==null){
				s=extractAfterVal(vv, "max=");
			}
			if(s!=null){
			sz.max=s;
			sz.valid=true;
			System.out.println("sz.valid=true");
			}
			return null;
		}
		if(vv.contains("xref") || vv.contains("ordered")){
			s="@"+capitalize(v);
 
		}
		if(s==null){
			s="//@"+v;
		}
		System.out.println("anyAnnot:"+v+":"+s);
		return s;
	}
	
 	public static String capitalize(String sentence) {
	        String words[] = sentence.replaceAll("\\s+", " ").trim().split(" ");
	        String newSentence = "";
	        for (String word : words) {
	            for (int i = 0; i < word.length(); i++)
	                newSentence = newSentence + ((i == 0) ? word.substring(i, i + 1).toUpperCase(): 
	                    (i != word.length() - 1) ? word.substring(i, i + 1).toLowerCase() : word.substring(i, i + 1).toLowerCase().toLowerCase() + " ");
	        }

	        return newSentence;
	    }
	public static String sizeAnnot(DataSize sz){
		String s0="";
		//"min = 10, max = 200";
		if(sz.min!=null){
			if(s0.length()>0){
				s0=s0+",";
			}
			s0="min = "+sz.min+" ";
		}
		
		
		if(sz.max!=null){
			if(s0.length()>0){
				s0=s0+",";
			}
			s0="max = "+sz.max+" ";
		}
		String s="@Size("+s0+")";
		 /*  @Size(min = 10, max = 200, message  = "About Me must be between 10 and 200 characters")*/
		return s;
	}

	private static String extractAfterVal(String v, String chr) {
		if(v.indexOf(chr)>=0){
		 
			String r= v.substring(v.indexOf(chr)+chr.length(),v.length());
	 
			return r;
		}else{
			return null;
		}
	
	}

	
	private static String extractUnitVal(String v, String chr) {
		if(v.indexOf(chr)>=0){
		 
			String r= v.substring(v.indexOf(chr)+5,v.length());
	 
			return r;
		}else{
			return null;
		}
	
	}
	/*
	       [%for(prop in self.getRedefinedProperties()){%]
         === [%=futils.displayClass(prop)%] 
        	   @[%=prop.name%] 
               [%=futils.anyAnnotSplit(prop.name)%]
      [%} %]
	 */
	 
	  public static String anyAnnotSplitList(List<PropertyImpl> redefinedProperties ){
		  String r="";
		  for(PropertyImpl prop : redefinedProperties ){
			  String v=anyAnnotSplit(prop.getName());
			  System.out.println("==<<HARD_DEBUG:prop.getName"+prop.getName()+">>");
			  for(Comment c:prop.getOwnedComments()){
					v+=anyAnnotSplit(c.getBody().trim());
				}
			  r+= v;
		  }
		  return r;
	  }
	  //PropertyImpl p
	  
	  
	  public static String anyDescSplitList(List<PropertyImpl> redefinedProperties ){
		  String r="";
		  for(PropertyImpl prop : redefinedProperties ){
			  String v = description(prop);
			  System.out.println("==<<HARD_DEBUG:anyDescSplitList:prop.getName"+prop.getName()+"==>"+v+"");
		      r+= v;
		  }
		  System.out.println(""+r);
		  return r;
	  }

	public static String description(PropertyImpl prop) {
		String v=anyDescSplit(prop.getName());
			for(Comment c:prop.getOwnedComments()){
				 System.out.println("BODY:"+c.getBody().trim());
				v+=anyDescSplit(c.getBody().trim());
			}
		return v;
	}
	 
	
	  
	    public static String anyDescSplit(String name){
	    //	System.out.println("DEB1:0.1");
	    	String descall="";
			String desc="";
			String desc2="";
			String desc3="";
			String enu="";
			String sep=",";
			String att="";
			int i=0;
	    	
	    	String r="";
	    	if(!name.contains("<Enter constraint text here>")){
	    		//System.out.println("DEB1:0.2"); 
	    		for(  String pname : name.trim().split("\\n")  ){
	    		//	System.out.println("DEB1:0.3");
		    		 
 
						String b=pname;
						if(b!=null && b.length()>0){
							String bb=b.replaceAll("\\s", "");
							bb=bb.toLowerCase().trim();
							
							if(bb.contains("desc:")
								|| bb.contains("desc2:")
								|| bb.contains("desc3:")
									){
								//desc+=b.replaceAll("desc:", "");	
							      descall+=b;
							     // System.out.println("DEB1:0.4");
						    		 
							}

							if(bb.contains("enum:")){
								enu+=b.replaceAll("enum:", "");	
							}
							
						}
					
		    		 
		    		 /*
		    	         String a=description(pname);
		    	         if(a!=null){
		    	          r+=a+"\n";
		    	         }
		    	    */      
		    	 }
		    	  
	    	   }
	    	 
	    	
	    	
	    	
	    	
	    	
			//return r;
	    	
	    	
			//description a separer en 3 : 
			//  desc, desc2 , desc3
				//separator : //n
				
				if(!descall.equals("")){
System.out.println("DEB1:1");
					descall=descall.replaceAll("\"", "'");
					descall = descall.replaceAll("\\r", "");
					String SEP2="ZaeRw1";
					descall=descall.replace("desc:", SEP2+"desc:");
					descall=descall.replace("desc2:", SEP2+"desc2:");
					descall=descall.replace("desc3:", SEP2+"desc3:");
					
					String[] vl1 = descall.split("\\n");
					for(String el1:vl1) {
						String[] vl = el1.split(SEP2);
					   for(String el:vl) {
						//if(el.indexOf("desc:")>=0) {}
						if(el.contains("desc:")){
							desc+=el.replaceAll("desc:", "");	
						}
						if(el.contains("desc2:")){
							desc2+=el.replaceAll("desc2:", "");	
						}
						if(el.contains("desc3:")){
							desc3+=el.replaceAll("desc3:", "");	
						}
					}}
					 // v = desc.replaceAll("\\n", ";");
					//desc=v.replaceAll("\"", "'");
					
					
					String attV=  " description=\""+desc+"\"";
					if(!desc2.equals("")) { attV+=", description2=\""+desc2+"\"";}
					if(!desc3.equals("")) { attV+=", description3=\""+desc3+"\"";}
					
					if(i>0){
						System.out.println("DEB1:2");
						att=att+sep+attV;}
					else{
						System.out.println("DEB1:3");
						att=att+attV;
					}
					i++;
					
					
				}
				if(!enu.equals("")){
					String v = enu.replaceAll("\\n", ";");
					enu=v.replaceAll("\"", "'");
					String attV="enumerate=\""+enu+"\"";
					if(i>0){
						att=att+sep+attV;}
					else{
						att=att+attV;
					}
					i++;
				}
				
				if(!att.equals("")){
					return "@Info("+att+")";	
				}else{
					return "";
				}
		}
	    
	    
	    /*
		public static String description(PropertyImpl p){
			//
			String descall="";
			String desc="";
			String desc2="";
			String desc3="";
			String enu="";
			String sep=",";
			String att="";
			int i=0;
			for(Comment c:p.getOwnedComments()){
				String b=c.getBody().trim();
				if(b!=null && b.length()>0){
					String bb=b.replaceAll("\\s", "");
					bb=bb.toLowerCase().trim();
					
					if(bb.contains("desc:")){
						//desc+=b.replaceAll("desc:", "");	
					      descall+=b;
					}

					if(bb.contains("enum:")){
						enu+=b.replaceAll("enum:", "");	
					}
					
				}
			}
		//description a separer en 3 : 
		//  desc, desc2 , desc3
			//separator : //n
			
			if(!descall.equals("")){

				String v=descall.replaceAll("\"", "'");
				descall = descall.replaceAll("\\r", "");
				String[] vl = descall.split("\\n");
				
				for(String el:vl) {
					if(el.contains("desc:")){
						desc+=el.replaceAll("desc:", "");	
					}
					if(el.contains("desc2:")){
						desc2+=el.replaceAll("desc2:", "");	
					}
					if(el.contains("desc3:")){
						desc3+=el.replaceAll("desc3:", "");	
					}
				}
				 // v = desc.replaceAll("\\n", ";");
				//desc=v.replaceAll("\"", "'");
				
				
				String attV=  " description=\""+desc+"\"";
				if(!desc2.equals("")) { attV+=", description2=\""+desc2+"\"";}
				if(!desc3.equals("")) { attV+=", description3=\""+desc3+"\"";}
				
				if(i>0){
					att=att+sep+attV;}
				else{
					att=att+attV;
				}
				i++;
				
				
			}
			if(!enu.equals("")){
				String v = enu.replaceAll("\\n", ";");
				enu=v.replaceAll("\"", "'");
				String attV="enumerate=\""+enu+"\"";
				if(i>0){
					att=att+sep+attV;}
				else{
					att=att+attV;
				}
				i++;
			}
			if(!att.equals("")){
				return "@Info("+att+")";	
			}else{
				return "";
			}
			
		}
		
		*/
    public static String anyAnnotSplit(String name){
    	DataSize sz= new DataSize();
    	
    	String r="";
    	if(!name.contains("<Enter constraint text here>")){
	    	 for(  String pname : name.trim().split("\\n")  ){
	    	         
	    	         String a=anyAnnot(pname,sz);
	    	         if(a!=null){
	    	          r+=a+"\n";
	    	         }
	    	          
	    	 }
	    	  if(sz.valid==true){
	    		  System.out.println("$$S");
	    		  r+= sizeAnnot(sz);
	    	  }
    	   }
    	System.out.println("<<HARD_DEBUG:"+r+">>");
		return r;
	}
	
}
