package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class Nitrogen extends Gravimetry {
 //declarations
	    /******************
	     *0 !!!info catalyst 
	  
	   *2 public String catalyst;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration catalyst
	   
	     */ 
	   
	      
	      @Info(description="Catalyseur")
	      public String catalyst;
  
	    /******************
	     *0 !!!info measuredNitrogen 
	   *1 @enum:NT,NPN,NCN KISS
	  
	   *2 public String measuredNitrogen;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration measuredNitrogen
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @enum:NT,NPN,NCN */ 
	      //@enum:NT,NPN,NCN
	      @Info(description="Type d'azote dosé")
	      public String measuredNitrogen;
  
//getter
 
	    /**
	     * Gets the value of catalyst
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@6f152006 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getCatalyst() {
		return this.catalyst;
	}
      
 
	    /**
	     * Gets the value of measuredNitrogen
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@3a6f2de3 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getMeasuredNitrogen() {
		return this.measuredNitrogen;
	}
      
//setter
   
	    /**
	     * Sets the value of catalyst
	     */
	public void setCatalyst(String catalyst) {
		this.catalyst = catalyst;
	}
      
   
	    /**
	     * Sets the value of measuredNitrogen
	     */
	public void setMeasuredNitrogen(String measuredNitrogen) {
		this.measuredNitrogen = measuredNitrogen;
	}
      
}

