package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class HplcHormone extends Chromatography {
 //declarations
	    /******************
	     *0 !!!info Androstenone 
	   *1 @has unit: g/100g
	       min>0
	       nullable: NA, LD, LQ KISS
	  
	   *2 public EFloat Androstenone;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EFloat
	   *7 		float
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration Androstenone
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @has unit: g/100g
	         	    min>0
	         	    nullable: NA, LD, LQ */ 
	      @CustomUnit(value="g/100g" )
	      @Null
	      @Size(min = 0 )
	      
	      public Float androstenone;
  
	    /******************
	     *0 !!!info Scatol 
	   *1 @has unit: g/100g
	       min>0
	       nullable: NA, LD, LQ KISS
	  
	   *2 public EFloat Scatol;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EFloat
	   *7 		float
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration Scatol
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @has unit: g/100g
	         	    min>0
	         	    nullable: NA, LD, LQ */ 
	      @CustomUnit(value="g/100g" )
	      @Null
	      @Size(min = 0 )
	      
	      public Float scatol;
  
	    /******************
	     *0 !!!info Indole 
	   *1 @has unit: g/100g
	       min>0
	       nullable: NA, LD, LQ KISS
	  
	   *2 public EFloat Indole;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EFloat
	   *7 		float
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration Indole
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @has unit: g/100g
	         	    min>0
	         	    nullable: NA, LD, LQ */ 
	      @CustomUnit(value="g/100g" )
	      @Null
	      @Size(min = 0 )
	      
	      public Float indole;
  
//getter
 
	    /**
	     * Gets the value of Androstenone
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@62452cc9 (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EFloat
	      
	     */
	public Float getAndrostenone() {
		return this.androstenone;
	}
      
 
	    /**
	     * Gets the value of Scatol
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@5bc9ba1d (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EFloat
	      
	     */
	public Float getScatol() {
		return this.scatol;
	}
      
 
	    /**
	     * Gets the value of Indole
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@488eb7f2 (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EFloat
	      
	     */
	public Float getIndole() {
		return this.indole;
	}
      
//setter
   
	    /**
	     * Sets the value of Androstenone
	     */
	public void setAndrostenone(Float androstenone) {
		this.androstenone = androstenone;
	}
      
   
	    /**
	     * Sets the value of Scatol
	     */
	public void setScatol(Float scatol) {
		this.scatol = scatol;
	}
      
   
	    /**
	     * Sets the value of Indole
	     */
	public void setIndole(Float indole) {
		this.indole = indole;
	}
      
}

