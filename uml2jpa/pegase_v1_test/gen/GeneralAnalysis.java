package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class GeneralAnalysis  {
 //declarations
	    /******************
	     *0 !!!info date 
	   *1 @ KISS
	  
	   *2 public EDate date;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EDate 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EDate
	   *7 		date
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration date
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @ */ 
	      //@
	      @Info(description="Date format jj/mm/aaa")
	      public Date date;
  
	    /******************
	     *0 !!!info operator 
	  
	   *2 public String operator;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration operator
	   
	     */ 
	   
	      
	      @Info(description="Manipulateur (LDAP)")
	      public String operator;
  
	    /******************
	     *0 !!!info laboratoryOperatingMode 
	  
	   *2 public String laboratoryOperatingMode;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration laboratoryOperatingMode
	   
	     */ 
	   
	      
	      @Info(description="Mode Operatoire de Laboratoire (MOL)")
	      public String laboratoryOperatingMode;
  
	    /******************
	     *0 !!!info criticalApparatusCriticalSoftware 
	  
	   *2 public String criticalApparatusCriticalSoftware;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration criticalApparatusCriticalSoftware
	   
	     */ 
	   
	      
	      @Info(description="Appareil ou logiciel critique (si plusieurs utiliser des ';')")
	      public String criticalApparatusCriticalSoftware;
  
	    /******************
	     *0 !!!info criticalProductReference 
	  
	   *2 public String criticalProductReference;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration criticalProductReference
	   
	     */ 
	   
	      
	      @Info(description="Fournisseur et ref. produits critiques (si plusieurs utiliser des ';')")
	      public String criticalProductReference;
  
	    /******************
	     *0 !!!info criticalProductLot 
	  
	   *2 public String criticalProductLot;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration criticalProductLot
	   
	     */ 
	   
	      
	      @Info(description="FNumero de lot produits critiques (si plusieurs utiliser des ';')")
	      public String criticalProductLot;
  
	    /******************
	     *0 !!!info rawDataPathway 
	  
	   *2 public String rawDataPathway;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration rawDataPathway
	   
	     */ 
	   
	      
	      @Info(description=" Lieu de stockage des donnees brutes")
	      public String rawDataPathway;
  
	    /******************
	     *0 !!!info sampleID 
	  
	   *2 public String sampleID;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration sampleID
	   
	     */ 
	   
	      
	      @Info(description="Identifiant de l'echantillon")
	      public String sampleID;
  
	    /******************
	     *0 !!!info comment 
	   *1 @Ordered(rindex=1) KISS
	  
	   *2 public String comment;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration comment
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(rindex=1) */ 
	      @Ordered(rindex=1)
	      @Info(description="Commentaire")
	      public String comment;
  
//getter
 
	    /**
	     * Gets the value of date
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@75f95314 (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EDate)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EDate
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EDate
	      
	     */
	public Date getDate() {
		return this.date;
	}
      
 
	    /**
	     * Gets the value of operator
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@495fac5f (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getOperator() {
		return this.operator;
	}
      
 
	    /**
	     * Gets the value of laboratoryOperatingMode
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@4dd02341 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getLaboratoryOperatingMode() {
		return this.laboratoryOperatingMode;
	}
      
 
	    /**
	     * Gets the value of criticalApparatusCriticalSoftware
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@55dfcc6 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getCriticalApparatusCriticalSoftware() {
		return this.criticalApparatusCriticalSoftware;
	}
      
 
	    /**
	     * Gets the value of criticalProductReference
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@7203c7ff (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getCriticalProductReference() {
		return this.criticalProductReference;
	}
      
 
	    /**
	     * Gets the value of criticalProductLot
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@2a76840c (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getCriticalProductLot() {
		return this.criticalProductLot;
	}
      
 
	    /**
	     * Gets the value of rawDataPathway
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@7cf6a5f9 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getRawDataPathway() {
		return this.rawDataPathway;
	}
      
 
	    /**
	     * Gets the value of sampleID
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@2fc6f97f (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getSampleID() {
		return this.sampleID;
	}
      
 
	    /**
	     * Gets the value of comment
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@2d6764b2 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getComment() {
		return this.comment;
	}
      
//setter
   
	    /**
	     * Sets the value of date
	     */
	public void setDate(Date date) {
		this.date = date;
	}
      
   
	    /**
	     * Sets the value of operator
	     */
	public void setOperator(String operator) {
		this.operator = operator;
	}
      
   
	    /**
	     * Sets the value of laboratoryOperatingMode
	     */
	public void setLaboratoryOperatingMode(String laboratoryOperatingMode) {
		this.laboratoryOperatingMode = laboratoryOperatingMode;
	}
      
   
	    /**
	     * Sets the value of criticalApparatusCriticalSoftware
	     */
	public void setCriticalApparatusCriticalSoftware(String criticalApparatusCriticalSoftware) {
		this.criticalApparatusCriticalSoftware = criticalApparatusCriticalSoftware;
	}
      
   
	    /**
	     * Sets the value of criticalProductReference
	     */
	public void setCriticalProductReference(String criticalProductReference) {
		this.criticalProductReference = criticalProductReference;
	}
      
   
	    /**
	     * Sets the value of criticalProductLot
	     */
	public void setCriticalProductLot(String criticalProductLot) {
		this.criticalProductLot = criticalProductLot;
	}
      
   
	    /**
	     * Sets the value of rawDataPathway
	     */
	public void setRawDataPathway(String rawDataPathway) {
		this.rawDataPathway = rawDataPathway;
	}
      
   
	    /**
	     * Sets the value of sampleID
	     */
	public void setSampleID(String sampleID) {
		this.sampleID = sampleID;
	}
      
   
	    /**
	     * Sets the value of comment
	     */
	public void setComment(String comment) {
		this.comment = comment;
	}
      
}

