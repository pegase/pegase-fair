#!/bin/bash
 
model="example/pegase_v1/model.uml" 

modeltmp="example/modtmp.uml"

outdir="example/pegase_v1"

EGX=example/pegase_v1/codegen.egx 

bash generate_any.sh $model $modeltmp $outdir $EGX
