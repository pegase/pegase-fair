package uml2rdf.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Info {

	String description() default "" ;
	String description2() default "" ;
	String description3() default "" ;
	String enumerate() default "" ;
	

}