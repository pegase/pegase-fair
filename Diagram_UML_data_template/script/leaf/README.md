
## how to generate a leaf.json for template filtering with gitlab.ci

```
 bash build.sh 
 
 bash run.sh 
 
 cat leaf.json

```


###  details of run.sh :

```
#!/bin/bash
MODELJAR="../model.gen.pegase_v1.jar"
JSONOUT="leaf.json"
ENTITYPACKAGE="gen"
java -jar leaf.jar "$MODELJAR"   "$JSONOUT" "$ENTITYPACKAGE"
cat "$JSONOUT"
```

```
cat leaf.json 
[
  "Immunohistochemistry",
  "StarchPolarimetric",
  "ClassicPCR",
  "WesternBlot",
  "HplcHydrolisedAA",
  "HplcHormone",
  "Imaging",
  "TwoDimensionGelElectrophoresis",
  ...
]
```