import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.*;
import java.util.jar.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class LeafClassFinder {

    public static void main(String[] args) throws Exception {
        // Check if the required command-line arguments are provided
        if (args.length != 3) {
            System.out.println("Usage: java LeafClassFinder <path-to-model.jar> <output-json-file> <entities-package-name>");
            System.exit(1);
        }

        String jarPath = args[0];
        String outputJsonFile = args[1];
        String packageName = args[2];

        // Validate jar file existence
        File jarFile = new File(jarPath);
        if (!jarFile.exists()) {
            System.err.println("Jar file not found: " + jarPath);
            System.exit(1);
        }

        // Load the jar file dynamically using URLClassLoader
        URL jarUrl = jarFile.toURI().toURL();
        URLClassLoader classLoader = new URLClassLoader(new URL[]{jarUrl});

        // Open the jar file and scan all classes inside it
        List<String> allClassNames = getAllClassesInJar(jarPath, packageName);

        // Map to store class hierarchy
        Map<String, Class<?>> classMap = new HashMap<>();
        for (String className : allClassNames) {
            try {
                Class<?> clazz = classLoader.loadClass(className);
                classMap.put(className, clazz);
            } catch (ClassNotFoundException e) {
                System.err.println("Class not found: " + className);
            }
        }

        // Determine leaf classes (classes that are not extended by any other class)
        Set<String> leafClasses = findLeafClasses(classMap);
        
        // Write leaf classes to a JSON file using Gson
        //Gson gson = new Gson();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (Writer writer = new FileWriter(outputJsonFile)) {
            gson.toJson(leafClasses, writer);
            System.out.println("Leaf classes written to " + outputJsonFile);
        }
    }

    // Helper method to get all class names in the jar file under a specific package
    private static List<String> getAllClassesInJar(String jarFilePath, String packageName) throws IOException {
         
        System.out.println("packageName: "+packageName);
        List<String> classNames = new ArrayList<>();
        try (JarFile jarFile = new JarFile(jarFilePath)) {
            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                System.out.println("##entry: <"+entry.getName()+">");
                if (entry.getName().endsWith(".class") && entry.getName().startsWith(packageName.replace(".", "/"))) {
                    // Convert the entry name to fully qualified class name
                    String className = entry.getName().replace("/", ".").replace(".class", "");
                    
                    classNames.add(className);
                    //System.out.println("###FOUND: "+className);
                }
            }
        }
        return classNames;
    }

    // Helper method to find leaf classes
    private static Set<String> findLeafClasses(Map<String, Class<?>> classMap) {
        Set<String> leafClasses = new HashSet<>();  
        HashMap<String,Boolean> leafClassesBool = new HashMap()  ;
        for (Class<?> clazz : classMap.values()) {
            leafClassesBool.put(clazz.getName(),true);
         
        }

        for (Class<?> clazz : classMap.values()) {
            //leafClasses.remove(clazz.getName());
             
            Class<?> superclass = clazz.getSuperclass();
            if (superclass != null && classMap.containsKey(superclass.getName())) {
                //leafClasses.remove(superclass.getName());  // If a class has a subclass, it's not a leaf
                //System.out.println("###DELETE: "+superclass.getName());
                leafClassesBool.put(superclass.getName(),false);
            }else{

/*
                System.out.println("ADD: "+superclass.getName());

                //leafClasses.remove(superclass.getName());
                String sclassName = superclass.getSimpleName();
                System.out.println(">>>"+sclassName);
                sclassName = sclassName.substring(sclassName.lastIndexOf("/") + 1).replace(".class", "");
                leafClasses.add(sclassName);
   */

            }

            for (String cln : leafClassesBool.keySet()) {
               Boolean isLeaf= leafClassesBool.get(cln);
               String sclassName = cln.replace(".class", "");
               sclassName = sclassName.substring(sclassName.lastIndexOf(".") + 1);
               leafClasses.add(sclassName);
               System.out.println("==>>>"+sclassName+" : "+isLeaf);
             
            }
        }
        return leafClasses;
    }
}
