#!/bin/bash
MODELJAR="../model.gen.pegase_v1.jar"
JSONOUT="leaf.json"
ENTITYPACKAGE="gen"
java -jar leaf.jar "$MODELJAR"   "$JSONOUT" "$ENTITYPACKAGE"
cat "$JSONOUT"