package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class WesternBlot extends GelElectrophoresis {
 //declarations
	    /******************
	     *0 !!!info transferType 
	  
	   *2 public String transferType;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration transferType
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Mode de transfert", description2=" Format : texte", description3=" ex : electrotransfert")
	      public String transferType;
  
	    /******************
	     *0 !!!info transferCondition 
	  
	   *2 public String transferCondition;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration transferCondition
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Conditions de transfert", description2=" Format : texte", description3=" ex : 30 Volt - 90 mA")
	      public String transferCondition;
  
	    /******************
	     *0 !!!info transfertBuffer 
	  
	   *2 public String transfertBuffer;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration transfertBuffer
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Tampon de transfert", description2=" Format : texte", description3=" ex : 25 mM Tris, pH 8.3, 192 mM glycine,")
	      public String transfertBuffer;
  
	    /******************
	     *0 !!!info membrane 
	  
	   *2 public String membrane;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration membrane
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Type de membrane", description2=" Format : texte", description3=" ex : PVDF, nitrocellulose")
	      public String membrane;
  
	    /******************
	     *0 !!!info labelType 
	  
	   *2 public String labelType;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration labelType
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Type de marquage", description2=" Format : texte", description3=" ex : biotin")
	      public String labelType;
  
	    /******************
	     *0 !!!info imageAcquisitionParameters 
	  
	   *2 public String imageAcquisitionParameters;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration imageAcquisitionParameters
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Paramètre d'acquisition d'image", description2=" Format : texte", description3=" ex : ...")
	      public String imageAcquisitionParameters;
  
	    /******************
	     *0 !!!info firstAntibodyName 
	  
	   *2 public String firstAntibodyName;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration firstAntibodyName
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Nom de l'anticorps primaire", description2=" Format : texte", description3=" ex : Vinculin Recombinant Rabbit Monoclonal Antibody (42H89L44)")
	      public String firstAntibodyName;
  
	    /******************
	     *0 !!!info firstAntibodyDilutionFactor 
	  
	   *2 public String firstAntibodyDilutionFactor;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration firstAntibodyDilutionFactor
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Facteur de dilution de l'anticorps primaire", description2=" Format : texte", description3=" ex : 0,5 µg/mL")
	      public String firstAntibodyDilutionFactor;
  
	    /******************
	     *0 !!!info secondAntibodyName 
	  
	   *2 public String secondAntibodyName;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration secondAntibodyName
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Nom de l'anticorps secondaire", description2=" Format : texte", description3=" ex : Goat anti-Rabbit IgG (H+L) Secondary Antibody, HRP")
	      public String secondAntibodyName;
  
	    /******************
	     *0 !!!info secondAntibodyDilutionFactor 
	  
	   *2 public String secondAntibodyDilutionFactor;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration secondAntibodyDilutionFactor
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Facteur de dilution de l'anticorps secondaire", description2=" Format : texte", description3=" ex : 1/10 000")
	      public String secondAntibodyDilutionFactor;
  
//getter
 
	    /**
	     * Gets the value of transferType
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@1ce93c18 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getTransferType() {
		return this.transferType;
	}
      
 
	    /**
	     * Gets the value of transferCondition
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@46044faa (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getTransferCondition() {
		return this.transferCondition;
	}
      
 
	    /**
	     * Gets the value of transfertBuffer
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@19f9d595 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getTransfertBuffer() {
		return this.transfertBuffer;
	}
      
 
	    /**
	     * Gets the value of membrane
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@778ca8ef (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getMembrane() {
		return this.membrane;
	}
      
 
	    /**
	     * Gets the value of labelType
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@261d8190 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getLabelType() {
		return this.labelType;
	}
      
 
	    /**
	     * Gets the value of imageAcquisitionParameters
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@907f2b7 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getImageAcquisitionParameters() {
		return this.imageAcquisitionParameters;
	}
      
 
	    /**
	     * Gets the value of firstAntibodyName
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@7dc51783 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getFirstAntibodyName() {
		return this.firstAntibodyName;
	}
      
 
	    /**
	     * Gets the value of firstAntibodyDilutionFactor
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@65e7f52a (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getFirstAntibodyDilutionFactor() {
		return this.firstAntibodyDilutionFactor;
	}
      
 
	    /**
	     * Gets the value of secondAntibodyName
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@107e5441 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getSecondAntibodyName() {
		return this.secondAntibodyName;
	}
      
 
	    /**
	     * Gets the value of secondAntibodyDilutionFactor
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@1f14f20c (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getSecondAntibodyDilutionFactor() {
		return this.secondAntibodyDilutionFactor;
	}
      
//setter
   
	    /**
	     * Sets the value of transferType
	     */
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
      
   
	    /**
	     * Sets the value of transferCondition
	     */
	public void setTransferCondition(String transferCondition) {
		this.transferCondition = transferCondition;
	}
      
   
	    /**
	     * Sets the value of transfertBuffer
	     */
	public void setTransfertBuffer(String transfertBuffer) {
		this.transfertBuffer = transfertBuffer;
	}
      
   
	    /**
	     * Sets the value of membrane
	     */
	public void setMembrane(String membrane) {
		this.membrane = membrane;
	}
      
   
	    /**
	     * Sets the value of labelType
	     */
	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}
      
   
	    /**
	     * Sets the value of imageAcquisitionParameters
	     */
	public void setImageAcquisitionParameters(String imageAcquisitionParameters) {
		this.imageAcquisitionParameters = imageAcquisitionParameters;
	}
      
   
	    /**
	     * Sets the value of firstAntibodyName
	     */
	public void setFirstAntibodyName(String firstAntibodyName) {
		this.firstAntibodyName = firstAntibodyName;
	}
      
   
	    /**
	     * Sets the value of firstAntibodyDilutionFactor
	     */
	public void setFirstAntibodyDilutionFactor(String firstAntibodyDilutionFactor) {
		this.firstAntibodyDilutionFactor = firstAntibodyDilutionFactor;
	}
      
   
	    /**
	     * Sets the value of secondAntibodyName
	     */
	public void setSecondAntibodyName(String secondAntibodyName) {
		this.secondAntibodyName = secondAntibodyName;
	}
      
   
	    /**
	     * Sets the value of secondAntibodyDilutionFactor
	     */
	public void setSecondAntibodyDilutionFactor(String secondAntibodyDilutionFactor) {
		this.secondAntibodyDilutionFactor = secondAntibodyDilutionFactor;
	}
      
}

