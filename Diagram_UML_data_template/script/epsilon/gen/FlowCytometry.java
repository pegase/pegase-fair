package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class FlowCytometry extends GeneralAnalysis {
 //declarations
	    /******************
	     *0 !!!info SSC 
	  
	   *2 public String SSC;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration SSC
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Paramètre lumière diffractée (SSC)", description2=" Format : texte", description3=" ex : ...")
	      public String sSC;
  
	    /******************
	     *0 !!!info FSC 
	  
	   *2 public String FSC;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration FSC
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Paramètre lumière diffractée (SSC)", description2=" Format : texte", description3=" ex : ...")
	      public String fSC;
  
	    /******************
	     *0 !!!info particlesFilter 
	  
	   *2 public String particlesFilter;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration particlesFilter
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Filtre utilisé", description2=" Format : texte", description3=" ex : ...")
	      public String particlesFilter;
  
	    /******************
	     *0 !!!info labelling 
	  
	   *2 public String labelling;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration labelling
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Marquage", description2=" Format : texte", description3=" ex : ...")
	      public String labelling;
  
	    /******************
	     *0 !!!info antibodyName 
	  
	   *2 public String antibodyName;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration antibodyName
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Nom de l'anticorps", description2=" Format : texte", description3=" ex :")
	      public String antibodyName;
  
	    /******************
	     *0 !!!info resultCellCount 
	  
	   *2 public Integer resultCellCount;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration resultCellCount
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Nombre de cellules comptées", description2=" Format : nombre entier", description3=" ex : ...")
	      public Integer resultCellCount;
  
//getter
 
	    /**
	     * Gets the value of SSC
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@fac80 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getSSC() {
		return this.sSC;
	}
      
 
	    /**
	     * Gets the value of FSC
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@14bb2297 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getFSC() {
		return this.fSC;
	}
      
 
	    /**
	     * Gets the value of particlesFilter
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@1a15b789 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getParticlesFilter() {
		return this.particlesFilter;
	}
      
 
	    /**
	     * Gets the value of labelling
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@6c4f9535 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getLabelling() {
		return this.labelling;
	}
      
 
	    /**
	     * Gets the value of antibodyName
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@499b2a5c (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getAntibodyName() {
		return this.antibodyName;
	}
      
 
	    /**
	     * Gets the value of resultCellCount
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@241a53ef (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getResultCellCount() {
		return this.resultCellCount;
	}
      
//setter
   
	    /**
	     * Sets the value of SSC
	     */
	public void setSSC(String sSC) {
		this.sSC = sSC;
	}
      
   
	    /**
	     * Sets the value of FSC
	     */
	public void setFSC(String fSC) {
		this.fSC = fSC;
	}
      
   
	    /**
	     * Sets the value of particlesFilter
	     */
	public void setParticlesFilter(String particlesFilter) {
		this.particlesFilter = particlesFilter;
	}
      
   
	    /**
	     * Sets the value of labelling
	     */
	public void setLabelling(String labelling) {
		this.labelling = labelling;
	}
      
   
	    /**
	     * Sets the value of antibodyName
	     */
	public void setAntibodyName(String antibodyName) {
		this.antibodyName = antibodyName;
	}
      
   
	    /**
	     * Sets the value of resultCellCount
	     */
	public void setResultCellCount(Integer resultCellCount) {
		this.resultCellCount = resultCellCount;
	}
      
}

