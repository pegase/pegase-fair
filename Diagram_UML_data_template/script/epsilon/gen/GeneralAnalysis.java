package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class GeneralAnalysis  {
 //declarations
	    /******************
	     *0 !!!info analysisDate 
	   *1 @Ordered(index=1) KISS
	  
	   *2 public EDate analysisDate;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EDate 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EDate
	   *7 		date
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration analysisDate
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=1) */ 
	      @Ordered(index=1) 
	 
	      
	      @Info( description=" Date de l'analyse", description2=" Format : date jj/mm/aaa", description3=" ex : 12/06/2019")
	      public Date analysisDate;
  
	    /******************
	     *0 !!!info operator 
	   *1 @Ordered(index=2) KISS
	  
	   *2 public String operator;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration operator
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=2) */ 
	      @Ordered(index=2) 
	 
	      
	      @Info( description=" Manipulateur", description2=" Format : texte, identifiant LDAP", description3=" ex : fherault")
	      public String operator;
  
	    /******************
	     *0 !!!info laboratoryOperatingModeCode 
	   *1 @Ordered(index=3) KISS
	  
	   *2 public String laboratoryOperatingModeCode;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration laboratoryOperatingModeCode
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=3) */ 
	      @Ordered(index=3) 
	 
	      
	      @Info( description=" Code du mode opératoire de laboratoire", description2=" Format : texte, MO-LAB-XXX, si plusieurs MO-LAB les séparer par des ' ; '", description3=" ex:  MO-LAB-029 ; MO-LAB-0235 ou NA si le mode opératoire n'existe pas")
	      public String laboratoryOperatingModeCode;
  
	    /******************
	     *0 !!!info laboratoryOperatingModeName 
	   *1 @Ordered(index=4) KISS
	  
	   *2 public String laboratoryOperatingModeName;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration laboratoryOperatingModeName
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=4) */ 
	      @Ordered(index=4) 
	 
	      
	      @Info( description=" Nom du mode opératoire de laboratoire", description2=" Format : texte, si plusieurs MO-LAB  les séparer par des ' ; '", description3=" ex : Dosage du Glutathion dans les tissus ou NA si le mode opératoire n'existe pas")
	      public String laboratoryOperatingModeName;
  
	    /******************
	     *0 !!!info criticalApparatusCriticalSoftware 
	   *1 @Ordered(index=5) KISS
	  
	   *2 public String criticalApparatusCriticalSoftware;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration criticalApparatusCriticalSoftware
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=5) */ 
	      @Ordered(index=5) 
	 
	      
	      @Info( description=" Appareil ou logiciel critique", description2=" Format : texte, si plusieurs appareils/logiciels les séparer par des ' ; '", description3=" ex : Konélab20 ; appareil 2 ; appareil 3")
	      public String criticalApparatusCriticalSoftware;
  
	    /******************
	     *0 !!!info criticalProductReference 
	   *1 @Ordered(index=6) KISS
	  
	   *2 public String criticalProductReference;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration criticalProductReference
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=6) */ 
	      @Ordered(index=6) 
	 
	      
	      @Info( description=" Fournisseur(s) et référence(s) du/des produit(s) critique(s)", description2=" Format : texte, si plusieurs produits les séparer par des ' ; '", description3=" ex : THERMO 981304 ; sCal 981831 ; Nortrol 981043 ; Abtrol 981044")
	      public String criticalProductReference;
  
	    /******************
	     *0 !!!info criticalProductLot 
	   *1 @Ordered(index=7) KISS
	  
	   *2 public String criticalProductLot;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration criticalProductLot
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=7) */ 
	      @Ordered(index=7) 
	 
	      
	      @Info( description=" Numéro de lot du/des produit(s) critique(s)", description2=" Format : texte, si plusieurs produits les séparer par des ' ; ' Conserver le même ordre que dans le champ 'CriticalProductReference'.", description3=" ex : S415 ; G716 ; H342 ; H654")
	      public String criticalProductLot;
  
	    /******************
	     *0 !!!info rawDataPathway 
	   *1 @Ordered(index=8) KISS
	  
	   *2 public String rawDataPathway;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration rawDataPathway
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=8) */ 
	      @Ordered(index=8) 
	 
	      
	      @Info( description=" Lieu de stockage des données brutes.", description2=" Format : texte", description3=" ex : //Konelab37/results/")
	      public String rawDataPathway;
  
	    /******************
	     *0 !!!info comment 
	   *1 @Ordered(index=9) KISS
	  
	   *2 public String comment;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration comment
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=9) */ 
	      @Ordered(index=9) 
	 
	      
	      @Info( description=" Commentaire", description2=" Format : texte libre", description3=" Ex : 'Quantité insuffisante d'échantillon' ou 'Echantillon dégradé'")
	      public String comment;
  
	    /******************
	     *0 !!!info experiment 
	   *1 @Ordered(index=10) KISS
	  
	   *2 public String experiment;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration experiment
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=10) */ 
	      @Ordered(index=10) 
	 
	      
	      @Info( description=" Identifiant de l'expérimentation", description2=" Format : texte", description3=" ex : P24-100")
	      public String experiment;
  
	    /******************
	     *0 !!!info sampleID 
	   *1 @Ordered(index=11) KISS
	  
	   *2 public String sampleID;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration sampleID
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @Ordered(index=11) */ 
	      @Ordered(index=11) 
	 
	      
	      @Info( description=" Identifiant de l'échantillon", description2=" Format : texte", description3=" ex : PEGASE35245 ou XXX")
	      public String sampleID;
  
//getter
 
	    /**
	     * Gets the value of analysisDate
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@5ea502e0 (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EDate)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EDate
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EDate
	      
	     */
	public Date getAnalysisDate() {
		return this.analysisDate;
	}
      
 
	    /**
	     * Gets the value of operator
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@77b7ffa4 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getOperator() {
		return this.operator;
	}
      
 
	    /**
	     * Gets the value of laboratoryOperatingModeCode
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@41382722 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getLaboratoryOperatingModeCode() {
		return this.laboratoryOperatingModeCode;
	}
      
 
	    /**
	     * Gets the value of laboratoryOperatingModeName
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@210386e0 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getLaboratoryOperatingModeName() {
		return this.laboratoryOperatingModeName;
	}
      
 
	    /**
	     * Gets the value of criticalApparatusCriticalSoftware
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@6ce1f601 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getCriticalApparatusCriticalSoftware() {
		return this.criticalApparatusCriticalSoftware;
	}
      
 
	    /**
	     * Gets the value of criticalProductReference
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@6e33c391 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getCriticalProductReference() {
		return this.criticalProductReference;
	}
      
 
	    /**
	     * Gets the value of criticalProductLot
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@47747fb9 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getCriticalProductLot() {
		return this.criticalProductLot;
	}
      
 
	    /**
	     * Gets the value of rawDataPathway
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@2a7b6f69 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getRawDataPathway() {
		return this.rawDataPathway;
	}
      
 
	    /**
	     * Gets the value of comment
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@7db0565c (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getComment() {
		return this.comment;
	}
      
 
	    /**
	     * Gets the value of experiment
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@2a551a63 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getExperiment() {
		return this.experiment;
	}
      
 
	    /**
	     * Gets the value of sampleID
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@6cc0bcf6 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getSampleID() {
		return this.sampleID;
	}
      
//setter
   
	    /**
	     * Sets the value of analysisDate
	     */
	public void setAnalysisDate(Date analysisDate) {
		this.analysisDate = analysisDate;
	}
      
   
	    /**
	     * Sets the value of operator
	     */
	public void setOperator(String operator) {
		this.operator = operator;
	}
      
   
	    /**
	     * Sets the value of laboratoryOperatingModeCode
	     */
	public void setLaboratoryOperatingModeCode(String laboratoryOperatingModeCode) {
		this.laboratoryOperatingModeCode = laboratoryOperatingModeCode;
	}
      
   
	    /**
	     * Sets the value of laboratoryOperatingModeName
	     */
	public void setLaboratoryOperatingModeName(String laboratoryOperatingModeName) {
		this.laboratoryOperatingModeName = laboratoryOperatingModeName;
	}
      
   
	    /**
	     * Sets the value of criticalApparatusCriticalSoftware
	     */
	public void setCriticalApparatusCriticalSoftware(String criticalApparatusCriticalSoftware) {
		this.criticalApparatusCriticalSoftware = criticalApparatusCriticalSoftware;
	}
      
   
	    /**
	     * Sets the value of criticalProductReference
	     */
	public void setCriticalProductReference(String criticalProductReference) {
		this.criticalProductReference = criticalProductReference;
	}
      
   
	    /**
	     * Sets the value of criticalProductLot
	     */
	public void setCriticalProductLot(String criticalProductLot) {
		this.criticalProductLot = criticalProductLot;
	}
      
   
	    /**
	     * Sets the value of rawDataPathway
	     */
	public void setRawDataPathway(String rawDataPathway) {
		this.rawDataPathway = rawDataPathway;
	}
      
   
	    /**
	     * Sets the value of comment
	     */
	public void setComment(String comment) {
		this.comment = comment;
	}
      
   
	    /**
	     * Sets the value of experiment
	     */
	public void setExperiment(String experiment) {
		this.experiment = experiment;
	}
      
   
	    /**
	     * Sets the value of sampleID
	     */
	public void setSampleID(String sampleID) {
		this.sampleID = sampleID;
	}
      
}

