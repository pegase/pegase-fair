package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class TwoDimensionGelElectrophoresis extends GelElectrophoresis {
 //declarations
	    /******************
	     *0 !!!info stripIdentification 
	  
	   *2 public Integer stripIdentification;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration stripIdentification
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Identification du dépôt", description2=" Format : nombre entier", description3=" ex : 5")
	      public Integer stripIdentification;
  
	    /******************
	     *0 !!!info scanIdentification 
	  
	   *2 public String scanIdentification;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration scanIdentification
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Identification du scan", description2=" Format : texte", description3=" ex : Scan 2D du 22/05/2024")
	      public String scanIdentification;
  
	    /******************
	     *0 !!!info gelIdentification 
	  
	   *2 public String gelIdentification;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration gelIdentification
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Identification du gel", description2=" Format : texte", description3=" ex : Gel 2D expé P24-100")
	      public String gelIdentification;
  
	    /******************
	     *0 !!!info proteinExtractionProcedure 
	  
	   *2 public String proteinExtractionProcedure;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration proteinExtractionProcedure
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Procédure d'extraction des protéines", description2=" Format : texte", description3=" ex : MO-LAB-029")
	      public String proteinExtractionProcedure;
  
	    /******************
	     *0 !!!info imageResolution 
	  
	   *2 public String imageResolution;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration imageResolution
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Résolution de l'image", description2=" Formet : texte", description3=" ex : ...")
	      public String imageResolution;
  
	    /******************
	     *0 !!!info result 
	  
	   *2 public EFloat result;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EFloat
	   *7 		float
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration result
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Résultat", description2=" Format : nombre décimal", description3=" ex : 3,14")
	      public Float result;
  
//getter
 
	    /**
	     * Gets the value of stripIdentification
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@737edcfa (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getStripIdentification() {
		return this.stripIdentification;
	}
      
 
	    /**
	     * Gets the value of scanIdentification
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@36c54a56 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getScanIdentification() {
		return this.scanIdentification;
	}
      
 
	    /**
	     * Gets the value of gelIdentification
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@73386d72 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getGelIdentification() {
		return this.gelIdentification;
	}
      
 
	    /**
	     * Gets the value of proteinExtractionProcedure
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@584f5497 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getProteinExtractionProcedure() {
		return this.proteinExtractionProcedure;
	}
      
 
	    /**
	     * Gets the value of imageResolution
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@6df20ade (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getImageResolution() {
		return this.imageResolution;
	}
      
 
	    /**
	     * Gets the value of result
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@4943defe (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EFloat
	      
	     */
	public Float getResult() {
		return this.result;
	}
      
//setter
   
	    /**
	     * Sets the value of stripIdentification
	     */
	public void setStripIdentification(Integer stripIdentification) {
		this.stripIdentification = stripIdentification;
	}
      
   
	    /**
	     * Sets the value of scanIdentification
	     */
	public void setScanIdentification(String scanIdentification) {
		this.scanIdentification = scanIdentification;
	}
      
   
	    /**
	     * Sets the value of gelIdentification
	     */
	public void setGelIdentification(String gelIdentification) {
		this.gelIdentification = gelIdentification;
	}
      
   
	    /**
	     * Sets the value of proteinExtractionProcedure
	     */
	public void setProteinExtractionProcedure(String proteinExtractionProcedure) {
		this.proteinExtractionProcedure = proteinExtractionProcedure;
	}
      
   
	    /**
	     * Sets the value of imageResolution
	     */
	public void setImageResolution(String imageResolution) {
		this.imageResolution = imageResolution;
	}
      
   
	    /**
	     * Sets the value of result
	     */
	public void setResult(Float result) {
		this.result = result;
	}
      
}

