package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class Histology extends Imaging {
 //declarations
	    /******************
	     *0 !!!info embedding 
	   *1 @enum:Parafine,Frozen KISS
	  
	   *2 public String embedding;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration embedding
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @enum:Parafine,Frozen */ 
	      //@enum:Parafine,Frozen 
	 
	      
	      @Info( description=" Type d'inclusion", description2=" Format : texte", description3=" ex : Parafine")
	      public String embedding;
  
	    /******************
	     *0 !!!info fixation 
	  
	   *2 public String fixation;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration fixation
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Type de fixation", description2=" Format : texte", description3=" ex : ...")
	      public String fixation;
  
	    /******************
	     *0 !!!info sliceSize 
	  
	   *2 public Integer sliceSize;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration sliceSize
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Epaisseur de la coupe (µm)", description2=" format :integer", description3=" ex : 1")
	      public Integer sliceSize;
  
	    /******************
	     *0 !!!info magnification 
	  
	   *2 public Integer magnification;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration magnification
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Grossissement", description2=" Format : nombre entier", description3=" ex : 10")
	      public Integer magnification;
  
	    /******************
	     *0 !!!info pictureResolution 
	  
	   *2 public Integer pictureResolution;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration pictureResolution
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Resolution de l'image (pixel/µm)", description2=" Format : nombre entier", description3=" ex : 250")
	      public Integer pictureResolution;
  
	    /******************
	     *0 !!!info pictureName 
	  
	   *2 public String pictureName;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration pictureName
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Nom de l'image", description2=" Format : texte", description3=" ex : ...")
	      public String pictureName;
  
	    /******************
	     *0 !!!info organelleIdentification 
	  
	   *2 public String organelleIdentification;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration organelleIdentification
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Nom de l'organite", description2=" Format : texte", description3=" ex : mitochondrie")
	      public String organelleIdentification;
  
//getter
 
	    /**
	     * Gets the value of embedding
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@7cd1ac19 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getEmbedding() {
		return this.embedding;
	}
      
 
	    /**
	     * Gets the value of fixation
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@69c43e48 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getFixation() {
		return this.fixation;
	}
      
 
	    /**
	     * Gets the value of sliceSize
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@547e29a4 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getSliceSize() {
		return this.sliceSize;
	}
      
 
	    /**
	     * Gets the value of magnification
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@1b39fd82 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getMagnification() {
		return this.magnification;
	}
      
 
	    /**
	     * Gets the value of pictureResolution
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@588ab592 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getPictureResolution() {
		return this.pictureResolution;
	}
      
 
	    /**
	     * Gets the value of pictureName
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@2d8f2f3a (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getPictureName() {
		return this.pictureName;
	}
      
 
	    /**
	     * Gets the value of organelleIdentification
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@c074c0c (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getOrganelleIdentification() {
		return this.organelleIdentification;
	}
      
//setter
   
	    /**
	     * Sets the value of embedding
	     */
	public void setEmbedding(String embedding) {
		this.embedding = embedding;
	}
      
   
	    /**
	     * Sets the value of fixation
	     */
	public void setFixation(String fixation) {
		this.fixation = fixation;
	}
      
   
	    /**
	     * Sets the value of sliceSize
	     */
	public void setSliceSize(Integer sliceSize) {
		this.sliceSize = sliceSize;
	}
      
   
	    /**
	     * Sets the value of magnification
	     */
	public void setMagnification(Integer magnification) {
		this.magnification = magnification;
	}
      
   
	    /**
	     * Sets the value of pictureResolution
	     */
	public void setPictureResolution(Integer pictureResolution) {
		this.pictureResolution = pictureResolution;
	}
      
   
	    /**
	     * Sets the value of pictureName
	     */
	public void setPictureName(String pictureName) {
		this.pictureName = pictureName;
	}
      
   
	    /**
	     * Sets the value of organelleIdentification
	     */
	public void setOrganelleIdentification(String organelleIdentification) {
		this.organelleIdentification = organelleIdentification;
	}
      
}

