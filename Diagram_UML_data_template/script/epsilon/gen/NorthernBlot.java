package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class NorthernBlot extends GelElectrophoresis {
 //declarations
	    /******************
	     *0 !!!info transferType 
	  
	   *2 public String transferType;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration transferType
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Mode de transfert", description2=" Format : texte", description3=" ex : Passif par capillarité ou electrotransfert")
	      public String transferType;
  
	    /******************
	     *0 !!!info transferCondition 
	  
	   *2 public String transferCondition;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration transferCondition
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Conditions de transfert", description2=" Format : texte", description3=" ex : 40 minutes à 24 Volt")
	      public String transferCondition;
  
	    /******************
	     *0 !!!info transfertBuffer 
	  
	   *2 public String transfertBuffer;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration transfertBuffer
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Tampon de transfert", description2=" Format : texte", description3=" ex : 20X SSC")
	      public String transfertBuffer;
  
	    /******************
	     *0 !!!info membrane 
	  
	   *2 public String membrane;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration membrane
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Type de membrane", description2=" Format : texte", description3=" ex : PVDF, Nylon")
	      public String membrane;
  
	    /******************
	     *0 !!!info probeName 
	  
	   *2 public String probeName;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration probeName
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Nom de la sonde", description2=" Format : texte", description3=" ex : ApoB")
	      public String probeName;
  
	    /******************
	     *0 !!!info labelType 
	  
	   *2 public String labelType;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration labelType
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Type de marquage", description2=" Format : texte", description3=" ex : biotin")
	      public String labelType;
  
	    /******************
	     *0 !!!info imageAcquisitionParameters 
	  
	   *2 public String imageAcquisitionParameters;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration imageAcquisitionParameters
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Paramètre d'acquisition d'image", description2=" Format : texte", description3=" ex : ...")
	      public String imageAcquisitionParameters;
  
	    /******************
	     *0 !!!info molecularWeight 
	  
	   *2 public Integer molecularWeight;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration molecularWeight
	   
	     */ 
	   
	       
	 
	      
	      @Info( description="Poids moléculaire de la cible (bp)", description2=" Format : nombre entier", description3=" ex : 3000")
	      public Integer molecularWeight;
  
	    /******************
	     *0 !!!info resultQuantity 
	   *1 @has unit : µg KISS
	  
	   *2 public Integer resultQuantity;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration resultQuantity
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @has unit : µg */ 
	      //@has unit : µg 
	 
	      
	      @Info( description=" Quantité (ng)", description2=" Format : nombre entier", description3=" ex : 20")
	      public Integer resultQuantity;
  
//getter
 
	    /**
	     * Gets the value of transferType
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@530a8454 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getTransferType() {
		return this.transferType;
	}
      
 
	    /**
	     * Gets the value of transferCondition
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@36b6964d (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getTransferCondition() {
		return this.transferCondition;
	}
      
 
	    /**
	     * Gets the value of transfertBuffer
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@75201592 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getTransfertBuffer() {
		return this.transfertBuffer;
	}
      
 
	    /**
	     * Gets the value of membrane
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@282308c3 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getMembrane() {
		return this.membrane;
	}
      
 
	    /**
	     * Gets the value of probeName
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@3d9fc57a (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getProbeName() {
		return this.probeName;
	}
      
 
	    /**
	     * Gets the value of labelType
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@1af05b03 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getLabelType() {
		return this.labelType;
	}
      
 
	    /**
	     * Gets the value of imageAcquisitionParameters
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@5bbbdd4b (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getImageAcquisitionParameters() {
		return this.imageAcquisitionParameters;
	}
      
 
	    /**
	     * Gets the value of molecularWeight
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@4fdf8f12 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getMolecularWeight() {
		return this.molecularWeight;
	}
      
 
	    /**
	     * Gets the value of resultQuantity
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@5a6d5a8f (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getResultQuantity() {
		return this.resultQuantity;
	}
      
//setter
   
	    /**
	     * Sets the value of transferType
	     */
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
      
   
	    /**
	     * Sets the value of transferCondition
	     */
	public void setTransferCondition(String transferCondition) {
		this.transferCondition = transferCondition;
	}
      
   
	    /**
	     * Sets the value of transfertBuffer
	     */
	public void setTransfertBuffer(String transfertBuffer) {
		this.transfertBuffer = transfertBuffer;
	}
      
   
	    /**
	     * Sets the value of membrane
	     */
	public void setMembrane(String membrane) {
		this.membrane = membrane;
	}
      
   
	    /**
	     * Sets the value of probeName
	     */
	public void setProbeName(String probeName) {
		this.probeName = probeName;
	}
      
   
	    /**
	     * Sets the value of labelType
	     */
	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}
      
   
	    /**
	     * Sets the value of imageAcquisitionParameters
	     */
	public void setImageAcquisitionParameters(String imageAcquisitionParameters) {
		this.imageAcquisitionParameters = imageAcquisitionParameters;
	}
      
   
	    /**
	     * Sets the value of molecularWeight
	     */
	public void setMolecularWeight(Integer molecularWeight) {
		this.molecularWeight = molecularWeight;
	}
      
   
	    /**
	     * Sets the value of resultQuantity
	     */
	public void setResultQuantity(Integer resultQuantity) {
		this.resultQuantity = resultQuantity;
	}
      
}

