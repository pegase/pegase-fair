package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class DryMatterOrMineralQuantification extends Gravimetry {
 //declarations
	    /******************
	     *0 !!!info temperature 
	  
	   *2 public Integer temperature;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration temperature
	   
	     */ 
	   
	       
	 
	      
	      @Info( description="", description2=" Format : nombre entier", description3=" ex : 95")
	      public Integer temperature;
  
	    /******************
	     *0 !!!info dryingTime 
	  
	   *2 public Integer dryingTime;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration dryingTime
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Temps de séchage (min)", description2=" Format : nombre entier", description3=" ex : 120")
	      public Integer dryingTime;
  
	    /******************
	     *0 !!!info temperatureCycle 
	  
	   *2 public String temperatureCycle;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration temperatureCycle
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Cycle de températures (degré celsius, minutes)", description2=" Format : texte", description3=" ex : ...")
	      public String temperatureCycle;
  
	    /******************
	     *0 !!!info crucibleIdentification 
	  
	   *2 public Integer crucibleIdentification;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration crucibleIdentification
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Numéro du creuset", description2=" Format : nombre entier", description3=" ex : 5")
	      public Integer crucibleIdentification;
  
	    /******************
	     *0 !!!info crucibleWeight 
	   *1 @nullable : NA, LD, LQ
	       min>0 KISS
	  
	   *2 public EFloat crucibleWeight;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EFloat
	   *7 		float
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration crucibleWeight
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @nullable : NA, LD, LQ
	         	    min>0 */ 
	      @Null
	      @Size(min = 0 ) 
	 
	      
	      @Info( description=" Masse du creuset", description2=" Format: nombre décimal", description3=" ex : 56")
	      public Float crucibleWeight;
  
	    /******************
	     *0 !!!info crucibleSampleWeight 
	  
	   *2 public EFloat crucibleSampleWeight;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EFloat
	   *7 		float
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration crucibleSampleWeight
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Masse du creuset et de l'échantillon", description2=" Format : nombre décimal", description3=" ex : 75,6")
	      public Float crucibleSampleWeight;
  
	    /******************
	     *0 !!!info crucibleSampleWeight103 
	  
	   *2 public EFloat crucibleSampleWeight103;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EFloat
	   *7 		float
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration crucibleSampleWeight103
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Masse du creuset et de l'échantillon  après passage à 103°C", description2=" Format : nombre décimal", description3=" ex : 68,3")
	      public Float crucibleSampleWeight103;
  
	    /******************
	     *0 !!!info crucibleSampleWeight550 
	  
	   *2 public EFloat crucibleSampleWeight550;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: EFloat
	   *7 		float
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration crucibleSampleWeight550
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Masse du creuset et de l'échantillon après passage à 550°C", description2=" Format : nombre décimal", description3=" ex : 59,2")
	      public Float crucibleSampleWeight550;
  
	    /******************
	     *0 !!!info resultUnit 
	   *1 @ KISS
	  
	   *2 public String resultUnit;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration resultUnit
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @ */ 
	      //@ 
	 
	      
	      @Info( description=" Unité du résultat", description2=" Format : texte", description3=" ex : mg")
	      public String resultUnit;
  
//getter
 
	    /**
	     * Gets the value of temperature
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@10027fc9 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getTemperature() {
		return this.temperature;
	}
      
 
	    /**
	     * Gets the value of dryingTime
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@fff25f1 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getDryingTime() {
		return this.dryingTime;
	}
      
 
	    /**
	     * Gets the value of temperatureCycle
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@2ca47471 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getTemperatureCycle() {
		return this.temperatureCycle;
	}
      
 
	    /**
	     * Gets the value of crucibleIdentification
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@f31c0c6 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getCrucibleIdentification() {
		return this.crucibleIdentification;
	}
      
 
	    /**
	     * Gets the value of crucibleWeight
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@2e8ab815 (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EFloat
	      
	     */
	public Float getCrucibleWeight() {
		return this.crucibleWeight;
	}
      
 
	    /**
	     * Gets the value of crucibleSampleWeight
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@41394595 (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EFloat
	      
	     */
	public Float getCrucibleSampleWeight() {
		return this.crucibleSampleWeight;
	}
      
 
	    /**
	     * Gets the value of crucibleSampleWeight103
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@5769e7ae (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EFloat
	      
	     */
	public Float getCrucibleSampleWeight103() {
		return this.crucibleSampleWeight103;
	}
      
 
	    /**
	     * Gets the value of crucibleSampleWeight550
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@287f94b1 (eProxyURI: pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml#EFloat
	        
	          nofragment:pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml
	          fragment:EFloat
	      
	     */
	public Float getCrucibleSampleWeight550() {
		return this.crucibleSampleWeight550;
	}
      
 
	    /**
	     * Gets the value of resultUnit
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@62f87c44 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getResultUnit() {
		return this.resultUnit;
	}
      
//setter
   
	    /**
	     * Sets the value of temperature
	     */
	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}
      
   
	    /**
	     * Sets the value of dryingTime
	     */
	public void setDryingTime(Integer dryingTime) {
		this.dryingTime = dryingTime;
	}
      
   
	    /**
	     * Sets the value of temperatureCycle
	     */
	public void setTemperatureCycle(String temperatureCycle) {
		this.temperatureCycle = temperatureCycle;
	}
      
   
	    /**
	     * Sets the value of crucibleIdentification
	     */
	public void setCrucibleIdentification(Integer crucibleIdentification) {
		this.crucibleIdentification = crucibleIdentification;
	}
      
   
	    /**
	     * Sets the value of crucibleWeight
	     */
	public void setCrucibleWeight(Float crucibleWeight) {
		this.crucibleWeight = crucibleWeight;
	}
      
   
	    /**
	     * Sets the value of crucibleSampleWeight
	     */
	public void setCrucibleSampleWeight(Float crucibleSampleWeight) {
		this.crucibleSampleWeight = crucibleSampleWeight;
	}
      
   
	    /**
	     * Sets the value of crucibleSampleWeight103
	     */
	public void setCrucibleSampleWeight103(Float crucibleSampleWeight103) {
		this.crucibleSampleWeight103 = crucibleSampleWeight103;
	}
      
   
	    /**
	     * Sets the value of crucibleSampleWeight550
	     */
	public void setCrucibleSampleWeight550(Float crucibleSampleWeight550) {
		this.crucibleSampleWeight550 = crucibleSampleWeight550;
	}
      
   
	    /**
	     * Sets the value of resultUnit
	     */
	public void setResultUnit(String resultUnit) {
		this.resultUnit = resultUnit;
	}
      
}

