package gen;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import thewebsemantic.Id;
import thewebsemantic.RdfProperty;
import thewebsemantic.Namespace;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import thewebsemantic.binding.Jenabean;
import thewebsemantic.binding.RdfBean;
import javax.management.DescriptorKey;
import uml2rdf.utils.*;
  
	
@Namespace("http://inra/pegase#")
public class GelElectrophoresis extends Electrophoresis {
 //declarations
	    /******************
	     *0 !!!info ElectrophoresisBuffer 
	  
	   *2 public String ElectrophoresisBuffer;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration ElectrophoresisBuffer
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Tampon d'électrophorèse", description2=" Format : texte", description3=" ex : TAE 1X")
	      public String electrophoresisBuffer;
  
	    /******************
	     *0 !!!info electrophoresisCondition 
	  
	   *2 public String electrophoresisCondition;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration electrophoresisCondition
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Condition d'électrophorese", description2=" Format : texte", description3=" ex : 4h à 75 V")
	      public String electrophoresisCondition;
  
	    /******************
	     *0 !!!info gelType 
	  
	   *2 public String gelType;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration gelType
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Type de gel et concentration", description2=" Format : texte", description3=" ex : gel d'agarose à 0,7%")
	      public String gelType;
  
	    /******************
	     *0 !!!info ladderName 
	  
	   *2 public String ladderName;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration ladderName
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Marqueur de taille moléculaire", description2=" Format : texte", description3=" ex : 1 kb DNA Ladder")
	      public String ladderName;
  
	    /******************
	     *0 !!!info sampleLoadingAmount 
	   *1 @has unit : µg KISS
	  
	   *2 public Integer sampleLoadingAmount;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: Integer
	   *7 		int
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration sampleLoadingAmount
	   
	     */ 
	   
	       /* === org.eclipse.uml2.uml.internal.impl.PropertyImpl*/
	       /*	   @has unit : µg */ 
	      //@has unit : µg 
	 
	      
	      @Info( description=" Quantité de matériel déposé (unité ng)", description2=" Format : nombre entier", description3=" ex : 25")
	      public Integer sampleLoadingAmount;
  
	    /******************
	     *0 !!!info sampleType 
	  
	   *2 public String sampleType;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration sampleType
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Type de matériel biologique", description2=" Format : texte", description3=" ex : ARN, Protéines")
	      public String sampleType;
  
	    /******************
	     *0 !!!info staining 
	  
	   *2 public String staining;
	   *3 type.name : 
	   *4 type.name(proxyURL) : pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String 
	   *5 type.class : class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	   *6 myType: String
	   *7 		String
	  	 **
	    
	   *****************/
	
	    /**
	     * declaration staining
	   
	     */ 
	   
	       
	 
	      
	      @Info( description=" Coloration", description2=" Format : texte", description3=" ex : SYBR Safe DNA Gel Stain, Lumiprobe,  Coomassie Blue")
	      public String staining;
  
//getter
 
	    /**
	     * Gets the value of ElectrophoresisBuffer
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@31500940 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getElectrophoresisBuffer() {
		return this.electrophoresisBuffer;
	}
      
 
	    /**
	     * Gets the value of electrophoresisCondition
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@7249dadf (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getElectrophoresisCondition() {
		return this.electrophoresisCondition;
	}
      
 
	    /**
	     * Gets the value of gelType
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@1c25b8a7 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getGelType() {
		return this.gelType;
	}
      
 
	    /**
	     * Gets the value of ladderName
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@f8908f6 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getLadderName() {
		return this.ladderName;
	}
      
 
	    /**
	     * Gets the value of sampleLoadingAmount
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@63fd4873 (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#Integer
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:Integer
	      
	     */
	public Integer getSampleLoadingAmount() {
		return this.sampleLoadingAmount;
	}
      
 
	    /**
	     * Gets the value of sampleType
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@70e0accd (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getSampleType() {
		return this.sampleType;
	}
      
 
	    /**
	     * Gets the value of staining
	     
	     class:class org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl
	      classS:PrimitiveTypeImpl
	      typeToString:org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl@3aacf32a (eProxyURI: pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String)
	      typeName:
	      
	        eProxyURI:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#String
	        
	          nofragment:pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml
	          fragment:String
	      
	     */
	public String getStaining() {
		return this.staining;
	}
      
//setter
   
	    /**
	     * Sets the value of ElectrophoresisBuffer
	     */
	public void setElectrophoresisBuffer(String electrophoresisBuffer) {
		this.electrophoresisBuffer = electrophoresisBuffer;
	}
      
   
	    /**
	     * Sets the value of electrophoresisCondition
	     */
	public void setElectrophoresisCondition(String electrophoresisCondition) {
		this.electrophoresisCondition = electrophoresisCondition;
	}
      
   
	    /**
	     * Sets the value of gelType
	     */
	public void setGelType(String gelType) {
		this.gelType = gelType;
	}
      
   
	    /**
	     * Sets the value of ladderName
	     */
	public void setLadderName(String ladderName) {
		this.ladderName = ladderName;
	}
      
   
	    /**
	     * Sets the value of sampleLoadingAmount
	     */
	public void setSampleLoadingAmount(Integer sampleLoadingAmount) {
		this.sampleLoadingAmount = sampleLoadingAmount;
	}
      
   
	    /**
	     * Sets the value of sampleType
	     */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
      
   
	    /**
	     * Sets the value of staining
	     */
	public void setStaining(String staining) {
		this.staining = staining;
	}
      
}

