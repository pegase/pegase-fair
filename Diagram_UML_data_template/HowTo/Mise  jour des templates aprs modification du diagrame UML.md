# Mise à jour des templates après modification du diagrame UML

le jeudi 3 octobre 2019
MAJ 20/06/2024


------

Après modification du diagramme UML il est nécessaire de mettre à jour les templates mis à disposition des utilisateurs sur La forge MIA.
Au préalable s'assurer que le répertoire local du git soit à jour (*/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair*):
* 	Se placer dans le répertoire */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair*
* 	Ouvrir un terminal
* 	Faire un git status pour s'assurer que le répertoire est à jour
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/1_Verif_MAJ_Git.png)

* 	Si ce n'est pas le cas faire une mise à jour 
 	> git pull

## 1) Apporter des modifications dans le diagramme uml
Dans Modelio ouvrir le dossier "Diagram_UML_data_template"
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/1_Modif_Uml.png)

Ajouter des class : type d'analyse
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/2_Modif_Uml.png)

Ajouter des attributs : composant des analyses
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/3_Modif_Uml.png)

* Indiquer le nom et le type de l'attribut
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/4_Modif_Uml.png)
* Ajout de notes ou contraintes

+ - Description : 
+ - * desc: intitulé en français
+ - * desc2: format de la donnée
+ - * desc3: exemple
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/5_Modif_Uml.png)
+ - Comment : commentaire libre
+ - Contrainte : Ordered(index=11) précise l'ordre d'apparition des attribut au sein de la class
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/6_Modif_Uml.png)

Sauvegarder les modifications !
 
## 2) exporter le model au format uml.

![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/1-exportUML.png)

**/home/fherault/modelio/workspace/Diagram_UML_data_template/XMI/model.uml**

![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/2-exportUML.png)

## 3) Générer les templates

**<u>Remarque:</u>** Nécessite java 8 et ant


1. Copier le fichier */home/fherault/modelio/workspace/Diagram_UML_data_template/XMI/model.uml* dans le répertoire */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/XMI/*

2. Effacer le contenu du répertoire  */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/script/bin/gen*
3. Effacer le contenu du répertoire  */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/script/epsilon/gen*
4. se placer dans le répertoire */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/script*
5.  Lancer le premier script bash create1.sh
> ./create1.sh
6. Lancer le second script bash create2.sh
> ./create2.sh
7. Les templates sont généré dans le répertoire: */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/script/template_gen*


## 4) Placer les templates dans Git

1. copier les templates du répertoire */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/script/template_gen*dans le répertoire */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/template_pegase_v1*
2. se placer dans le répertoire */home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair*
3. faire un git:
- git status: pour voir les fichiers suivi et non suivis
 ![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/1_Git_MAJ_Template.png)
- git add pour ajouter des repertoires ou fichier
 > git add .
- git status: pour voir les fichiers suivi et non suivis
 ![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/2_Git_MAJ_Template.png)
- git commit -m “mon message” pour soumettre les modif à git
  ![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/3_Git_MAJ_Template.png)
- git push origin master pour mettre à jour le git
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/4_Git_MAJ_Template.png)

- git status: pour voir si les fichiers sont mis à jour
![](/home/fherault/Documents/RDO_RDS/PEGASE/FAIR-DS/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/1_Verif_MAJ_Git.png)
 



## 5) Mise à disposition des templates sur l’apli qualité

Une ligne par template.

![](/home/fherault/Documents/Qualité/Qualité_Maitrise_données/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/3-MiseADispoTemplateQualite.png)



**<u>ATTENTION:</u>** Il faut copier l’adresse du téléchargement sans le “https://”

![](/home/fherault/Documents/Qualité/Qualité_Maitrise_données/PEGASE_FAIR_MIA_GITLAB/pegase-fair/Diagram_UML_data_template/HowTo/PNG/4-AdresseTéléchargement.png)