HOSTNAME=data.inra.fr
API_TOKEN=8a985642-83a3-45c9-8a57-fb4809bfbec4

#PERSISTENT_ID=doi:10.15454/NCPSMX
#non assigne
PERSISTENT_ID=doi:10.15454/PGS50Y
#display
curl -u $API_TOKEN: https://$HOSTNAME/dvn/api/data-deposit/v1.1/swordv2/edit/study/$PERSISTENT_ID


#obtain dataset "database id 
curl -H "X-Dataverse-key:$API_TOKEN" -X GET https://$HOSTNAME/api/datasets/:persistentId/?persistentId=$PERSISTENT_ID  > zz && cat zz | jq '.data.id'


#publish file linked to an existing dataset
#DATAVERSE_ALIAS=doi:10.15454/OWJJUU
#curl -u $API_TOKEN: https://$HOSTNAME/dvn/api/data-deposit/v1.1/swordv2/collection/dataverse/$DATAVERSE_ALIAS

FILE1=data.tsv
FILE1_DESC=data.json
PERSISTENT_ID=doi:10.15454/NCPSMX
DESC= `cat $FILE1_DESC`
JSD="'jsonData="$DESC"'"
curl -H "X-Dataverse-key:$API_TOKEN" -X POST -F "file=@${FILE1}" -F $JSD "https://$HOSTNAME/api/datasets/:persistentId/add?persistentId=$PERSISTENT_ID"


#curl -H "X-Dataverse-key:$API_TOKEN" -X POST -F "file=@${FILE1}"  -F 'jsonData={"description":"My description.","categories":["Data"], "restrict":"true"}' "https://$HOSTNAME/api/datasets/:persistentId/add?persistentId=$PERSISTENT_ID"
