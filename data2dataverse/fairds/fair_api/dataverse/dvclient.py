from datetime import datetime
import json
import requests 
import os
import zipfile

class DataverseAPIClient:

    def __init__(self, server_url, api_key):
        self.server = server_url
        self.api_key = api_key





    def read_file(self,filename, mode='r'):
 
        try:
            with open(filename, mode) as f:
                data = f.read()
            return data
        except IOError:
            print('An error occured trying to read the file {}.'.format(filename))
        except Exception as e:
            raise e



    def filestruct(self,filepath,bn):
        
        if bn==False:
           file_content=""
           with open(filepath) as f:
            for line in f:
                file_content += line
        else:
           file_content =self.read_file(filepath,'rb')

        fil = {'file': (os.path.basename(filepath), file_content)}
        return fil

    def addfile2ds(self,did,filepath,params,bn=False,mode=2):

        if mode==1:
        # Add file using the Dataset's id
             urlr = '%s/api/datasets/%s/add?key=%s' % (self.server, did, self.api_key)
        else:
       # Add file using the Dataset's persistentId (e.g. doi, hdl, etc)
             urlr = '%s/api/datasets/:persistentId/add?persistentId=%s&key=%s' % (self.server, did,  self.api_key)

        params_as_json_string = json.dumps(params)

        payload = dict(jsonData=params_as_json_string)
 
        r = requests.post(urlr, data=payload, files=self.filestruct(filepath,bn))
        return r






    def deletefile2ds(self,did ):

      
        urlr = '%s/dvn/api/data-deposit/v1.1/swordv2/edit-media/file/%s' % (self.server, did)

        print(urlr)
        r = requests.delete(urlr,  auth =(  self.api_key  ,'')) 
 
        return r

    def displayds(self,did ):

      
        urlr = '%s/dvn/api/data-deposit/v1.1/swordv2/statement/study/%s' % (self.server, did)

        #print(urlr)
        r = requests.get(urlr, auth =(  self.api_key ,'')) 
 
        return r
 

    def auth_headers(self):
        headers = {
            'X-Dataverse-key':  self.api_key,
        }
        return headers

    def dsinfo(self,did ):


 
 
        urlr = '%s/api/datasets/:persistentId/?persistentId=%s' % (self.server, did)

        print(urlr)
        r = requests.get(urlr , headers=self.auth_headers()) 
 
        
        return r 
 

    def persitentid2db(self,did ):

 
 
        r = self.dsinfo(did)
 
        di = json.loads(r.text)
        dbid=di['data']['id']
        return dbid


    def zipfile(self,path,arch_name):
 
       ziph = zipfile.ZipFile(arch_name, 'w', zipfile.ZIP_DEFLATED)
       
       ziph.write(path)
 
       ziph.close()

    def zipdir(self,path,arch_name):
 
       ziph = zipfile.ZipFile(arch_name, 'w', zipfile.ZIP_DEFLATED)
       for root, dirs, files in os.walk(path):
            for file in files:
                ziph.write(os.path.join(root, file))
       ziph.close()


    def addzippedfile2ds(self,did,filepath):

        
        filename=os.path.basename(filepath)


        headers = {
            'Content-Disposition': 'filename=%s' %(filename),
            'Content-Type': 'application/zip',
            'Packaging': 'http://purl.org/net/sword/package/SimpleZip',
        }

        dataf = open(filepath, 'rb').read()
        urlr = '%s/dvn/api/data-deposit/v1.1/swordv2/edit-media/study/%s' % (self.server, did)
        r = requests.post(urlr, headers=headers, data=dataf, auth=(self.api_key, ''))
 
        return r

